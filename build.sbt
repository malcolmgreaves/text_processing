
name := "schnell"

version := "1.0"

organization := "edu.cmu.ml"

scalaVersion := "2.10.2"

retrieveManaged := true

crossPaths := false

resolvers ++= Seq(
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
  "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"
)


libraryDependencies ++= Seq(
  "com.google.code.gson" % "gson" % "2.2.4",
  "com.google.guava" % "guava" % "14.0.1",
  "com.google.protobuf" % "protobuf-java" % "2.5.0",
  "com.beust" % "jcommander" % "1.32",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.3.0",
  "de.bwaldvogel" % "liblinear" % "1.92",
  "org.apache.lucene" % "lucene-core" % "4.4.0",
  "org.apache.lucene" % "lucene-queries" % "4.4.0",
  "org.apache.lucene" % "lucene-analyzers" % "3.6.2",
  "org.apache.lucene" % "lucene-analyzers-common" % "4.4.0",
  "org.apache.lucene" % "lucene-queryparser" % "4.4.0",
  "org.apache.lucene" % "lucene-smartcn" % "3.6.2",
  "org.apache.commons" % "commons-compress" % "1.5",
  "org.apache.opennlp" % "opennlp" % "1.5.3",
  "org.apache.opennlp" % "opennlp-tools" % "1.5.3",
  "org.apache.opennlp" % "opennlp-maxent" % "3.0.3",
  "org.apache.opennlp" % "opennlp-docs" % "1.5.3",
  "org.apache.hadoop" % "hadoop-core" % "1.0.3",
  "org.apache.pig" % "pig" % "0.12.0",
  "log4j" % "log4j" % "1.2.16", 
  "org.apache.httpcomponents" % "httpclient" % "4.3.1",
  "org.apache.httpcomponents" % "httpmime" % "4.3.1",
  "org.maltparser" % "maltparser" % "1.7.2",
  "org.mockito" % "mockito-all" % "1.9.5",
  "org.riedelcastro" % "whatswrong" % "0.2.4",
  "net.sf.trove4j" % "trove4j" % "3.0.3",
  "org.scalanlp" % "breeze-core_2.10" % "0.4",
  "org.scalanlp" % "breeze-math_2.10" % "0.4",
  "org.scalanlp" % "nak" % "1.1.3",
  "org.scalanlp" % "chalk" % "1.2.0",
  "com.amazonaws" % "aws-java-sdk" % "1.6.8" exclude("org.codehaus.jackson","jackson-core-asl") //exclude("org.codehaus.jackson","jackson-mapper-asl")
)
            

