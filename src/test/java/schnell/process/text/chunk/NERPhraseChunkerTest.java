package schnell.process.text.chunk;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class NERPhraseChunker_Test {
  
  public TestPhraseChunker tester;
  
  @Before
  public void setUp(){
    this.tester = new TestPhraseChunker(
        new NERPhraseChunker(),
        Logger.getLogger(NERPhraseChunker_Test.class));
  }

  @Test
  public void PreserveSentenceOrderTest() {
    tester.PreserveSentenceOrderTest();
  }
  
  @Test 
  public void PreserveDependencyIndexSemanticsTest() {
   tester.PreserveDependencyIndexSemanticsTest();
  }
  
  @Test 
  public void EnsureValidDependencyIndicesTest() {
   tester.EnsureValidDependencyIndicesTest();
  }


  @Test public void ArtificialExampleTest(){
    tester.ArtificialExampleTest();
  }
  
}