package schnell.process.text.chunk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;

import schnell.data.read.TokenListIterator;
import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.tok.TokUtil;
import schnell.struct.text.tok.TokenPosNerDpOffset;
import schnell.util.Strings;

import com.google.common.collect.Lists;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 * @author Kathryn Mazaitis
 */
public class PhraseChunkerTest {

  public PhraseChunkerTest(PhraseChunker chunker, Logger Log){
    this.chunker = chunker;
    this.Log = Log;
  }

  protected PhraseChunker chunker;
  protected Logger Log;


  static final public String
  TEST_DATA_LOCATION = "data/test/phrase_chunking/ner/np_chunking_testdata.txt";

  /**CHECKS
   *   1. Original and Chunked sentences are the same.
   *     1a. All merging of tokens occurs correctly.
   */
  public void PreserveSentenceOrderTest() {
    Log.debug("\nTEST PreserveSentenceOrderTest\n");
    try{
      TokenListIterator iter = new TokenListIterator(new File(TEST_DATA_LOCATION));
      while(iter.hasNext()){
        List<TextPosNerDpOffset> tokens = iter.next();
        if(Log.isDebugEnabled()){
          Log.debug("O: "+tokens);
        } 
        PreserveSentenceOrderTest_h(tokens);
      }
    } catch(FileNotFoundException e){
      Assert.fail(e.getMessage());

    } catch (IOException e) {
      Assert.fail(e.getMessage());
    } 
  }

  static public void CheckChunkedSizeIsSmaller(
      List<TextPosNerDpOffset> original,
      List<TextPosNerDpOffset> chunked){
    if(original == null || chunked == null){
      Assert.fail("neither original nor chunked may be null");
    } else {
      Assert.assertEquals(true, original.size() > chunked.size());      
    }
  }

  protected void PreserveSentenceOrderTest_h(List<TextPosNerDpOffset> tokens) {
    String origSent = TokUtil.toOriginalSentence(tokens);
    Log.debug("O: "+origSent);

    List<TextPosNerDpOffset> chunked = chunker.chunk(tokens);
    String chunkSent = TokUtil.toOriginalSentence(chunked);
    Log.debug("C: "+chunkSent);

    if(!origSent.equals(chunkSent)){
      Assert.fail("sentences should be exact the same !!");
    }

    CheckChunkedSizeIsSmaller(tokens, chunked);

    Log.debug("PASS\n");
  }

  protected class TokenPoint {
    String depOrig, depChunk;
    TextPosNerDpOffset htok;
    TokenPoint(TextPosNerDpOffset htok,String depOrig, String depChunk){
      this.htok = htok;
      this.depOrig = depOrig;
      this.depChunk = depChunk;
    }
    boolean isValid(){
      return depOrig != null && depChunk != null && depChunk.contains(depOrig);
    }
    @Override public String toString(){
      return Strings.build("tok: ",htok,"  original dep: ",depOrig,
          "  curr dep: ",depChunk);
    }
  }

  /**CHECKS
   *   1. Dependency head indices are updated accordingly in the chunked
   *      sentence.
   */

  public void PreserveDependencyIndexSemanticsTest() {
    Log.debug("\nTEST PreserveDependencyIndexSemanticsTest\n");
    try{
      TokenListIterator iter = new TokenListIterator(new File(TEST_DATA_LOCATION));
      while(iter.hasNext()){
        List<TextPosNerDpOffset> tokens = iter.next();
        PreserveDependencyIndexSemanticsTest_h(tokens);
      }

    }catch(FileNotFoundException e){
      Assert.fail(e.getMessage());
    } catch (IOException e) {
      Assert.fail(e.getMessage());
    }
  }


  protected void PreserveDependencyIndexSemanticsTest_h(List<TextPosNerDpOffset> tokens){
    TextPosNerDpOffset htok;
    TokenPoint[] tps = new TokenPoint[tokens.size()];

    for(int ii = 0; ii < tokens.size(); ii++){
      htok = tokens.get(ii);

      if(htok.dependencyIndex() < 0 || htok.dependencyIndex() > tokens.size()){
        Assert.fail("invalid dependency head index (largest token index= "
            +tokens.size()+"), offending token:  "+htok);
      }

      if(htok.dependencyIndex() != 0){  
        tps[ii] = new TokenPoint(
            htok,
            tokens.get(htok.dependencyIndex()-1).text(), 
            null); 
      } else { 
        tps[ii] = null;
      }
    }

    Log.debug("O: "+TokUtil.toOriginalSentence(tokens));
    Log.debug("O: "+TokUtil.toTokenIndexedSentence(tokens));

    List<TextPosNerDpOffset> chunked = chunker.chunk(tokens);
    Log.debug("C: "+TokUtil.toOriginalSentence(chunked));
    Log.debug("C: "+TokUtil.toTokenIndexedSentence(chunked));

    for(int ii = 0; ii < tps.length; ii++){
      if(tps[ii] != null){
        for(int jj = 0; jj < chunked.size(); jj++){
          TextPosNerDpOffset tok = chunked.get(jj);

          if(tok.text().equals(tps[ii].htok.text())
              && tok.hashCode() - tps[ii].htok.hashCode() == 0){

            TextPosNerDpOffset dep = chunked.get(tok.dependencyIndex()-1);
            if(!dep.text().contains(tps[ii].depOrig)){
              System.err.println("\n\t"+"tok: "+tok);
              System.err.println("\t"+"depends:            "+dep.text());
              System.err.println("\t"+"supposed to depend: "+tps[jj].depOrig);
              System.err.println("\t"+"tps["+ii+"] = "+tps[jj]);

              System.err.flush();
              Assert.fail("broke dependency semantics: "+tps[jj]+"    D:"
                  +tok.toString());
            }
          }
        }
      }
    }

    CheckChunkedSizeIsSmaller(tokens, chunked);

    Log.debug("PASS\n");
  }

  /**CHECKS
   *   1. Dependency head indices are updated accordingly in the chunked
   *      sentence.
   */

  public void EnsureValidDependencyIndicesTest() {
    Log.debug("\nTEST EnsureValidDependencyIndicesTest\n");
    try{
      TokenListIterator iter = new TokenListIterator(new File(TEST_DATA_LOCATION));
      while(iter.hasNext()){
        List<TextPosNerDpOffset> tokens = iter.next();
        EnsureValidDependencyIndicesTest_h(tokens);
      }

    }catch(FileNotFoundException e){
      Assert.fail(e.getMessage());
    } catch (IOException e) {
      Assert.fail(e.getMessage());
    }
  }

  protected void EnsureValidDependencyIndicesTest_h(List<TextPosNerDpOffset> tokens){
    TextPosNerDpOffset htok;
    TokenPoint[] tps = new TokenPoint[tokens.size()];

    for(int ii = 0; ii < tokens.size(); ii++){
      htok = tokens.get(ii);
      if(htok.dependencyIndex() != 0){
        tps[ii] = new TokenPoint(
            htok,
            tokens.get(htok.dependencyIndex()-1).text(), 
            null); 
      } else { 
        tps[ii] = null;
      }
    }

    Log.debug("O: "+TokUtil.toOriginalSentence(tokens));
    Log.debug("O: "+TokUtil.toTokenIndexedSentence(tokens));

    List<TextPosNerDpOffset> chunked = chunker.chunk(tokens);
    Log.debug("C: "+TokUtil.toOriginalSentence(chunked));
    Log.debug("C: "+TokUtil.toTokenIndexedSentence(chunked));

    for(int ii = 0; ii < tps.length; ii++){
      if(tps[ii] != null){
        for(int jj = 0; jj < chunked.size(); jj++){
          TextPosNerDpOffset tok = chunked.get(jj);

          if(tok.text().equals(tps[ii].htok.text())
              && tok.hashCode() - tps[ii].htok.hashCode() == 0){

            TextPosNerDpOffset dep = chunked.get(tok.dependencyIndex()-1);
            if(!dep.text().contains(tps[ii].depOrig)){
              System.err.println("\n\t"+"tok: "+tok);
              System.err.println("\t"+"depends:            "+dep.text());
              System.err.println("\t"+"supposed to depend: "+tps[jj].depOrig);
              System.err.println("\t"+"tps["+ii+"] = "+tps[jj]);

              System.err.flush();
              Assert.fail("broke dependency semantics: "+tps[jj]+"    D:"
                  +tok.toString());
            }
          }
        }
      }
    }

    CheckChunkedSizeIsSmaller(tokens, chunked);

    Log.debug("PASS\n");
  }


  public void ArtificialExampleTest(){
    Log.debug("\nTEST ArtificialExample\n");
    List<TextPosNerDpOffset> tokens = Lists.newArrayList();
    tokens.add(new TokenPosNerDpOffset("Buck", "", "PERSON", "nn",2,-1,-1));
    tokens.add(new TokenPosNerDpOffset("Rodgers", "", "PERSON", "v", 3,-1,-1));
    tokens.add(new TokenPosNerDpOffset("went", "", "O", "v", 0,-1,-1));
    tokens.add(new TokenPosNerDpOffset("home", "", "O", "v", 3,-1,-1));
    tokens.add(new TokenPosNerDpOffset("to", "", "O", "v", 3,-1,-1));
    tokens.add(new TokenPosNerDpOffset("New", "", "LOCATION", "nn", 7,-1,-1));
    tokens.add(new TokenPosNerDpOffset("York", "", "LOCATION", "v", 3,-1,-1));

    Log.debug("buck rodgers:: "+tokens);

    PreserveDependencyIndexSemanticsTest_h(tokens);
  }
}