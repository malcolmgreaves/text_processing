package schnell.process.text.tag;

import java.io.File;
import java.io.IOException;
import java.util.List;


import org.junit.Test;



import schnell.data.io.IO;
import schnell.struct.text.tag.NERTagged;

public class StanfordNERTagger_Test {

  public StanfordNERTagger make()
      throws IOException, ClassNotFoundException{
    return StanfordNERTagger.make();
  }

  static final public String
  SAMPLE = "data/test/stanford_ner_tagger/sample.txt",
  SAMPLE_NER = "data/test/stanford_ner_tagger/sample.ner.txt";

  @Test
  public void test()
      throws IOException, ClassNotFoundException{
    StanfordNERTagger ner = make();
    String text = IO.readAll(new File(SAMPLE));
    List<NERTagged> tagged = ner.nerTag(text);
    
    System.out.println(tagged);
    
  }

}
