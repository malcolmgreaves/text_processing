package schnell.process.text.tag;

import java.io.IOException;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import schnell.data.TestingData;
import schnell.struct.text.tag.POSTagged;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class StanfordPOSTagger_Test {

  @Test
  public void test(){
    try{
      testposTagger(StanfordPOSTagger.make());
    }catch(IOException e){
      Assert.fail(String.format("should not have caught IOException:\n%s", Exceptions.toString(e)));
    }
  }
  
  public void testposTagger(POSTagger posTagger) {
    
    List<POSTagged> posTagged = posTagger.tag(TestingData.TEST_SENTENCE_1);
      
    Assert.assertEquals(5, posTagged.size());
    
    Assert.assertEquals("John", posTagged.get(0).text());
    Assert.assertEquals("NNP", posTagged.get(0).posTag());
    
    Assert.assertEquals("kicked", posTagged.get(1).text());
    Assert.assertEquals("VBD", posTagged.get(1).posTag());

    Assert.assertEquals("the", posTagged.get(2).text());
    Assert.assertEquals("DT", posTagged.get(2).posTag());
    
    Assert.assertEquals("bucket", posTagged.get(3).text());
    Assert.assertEquals("NN", posTagged.get(3).posTag());
    
    Assert.assertEquals(".", posTagged.get(4).text());
    Assert.assertEquals(".", posTagged.get(4).posTag());
  }

}