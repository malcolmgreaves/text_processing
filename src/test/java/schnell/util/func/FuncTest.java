package schnell.util.func;

import org.junit.Test;

/**
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class Func_Test {

	@Test
	public void Test0_RealValuedIdentityFunction() {
		System.out.println("Test0_RealValuedIdentityFunction");

		Func.Real rf = Func.Real.IDENTITY;
		System.out.println(String.format("%f = %f (1.000000)", 1.0, rf.evaluate(1)));
		System.out.println(String.format("%f = %f (6.256100)", 6.2561, rf.evaluate(6.2561)));
		System.out.println(String.format("equal? %b (true)", 1.0 == rf.evaluate(1.0)));

		System.out.println("\n\n");
	}

	@Test
	public void Test0_SigmoidFunction() {
		System.out.println("Test0_SigmoidFunction");
		double x;
		Func.Real rf = Func.Real.SIGMOID;

		x = 0.124;
		System.out.println(String.format("exp(%f) / 1 + exp(%f) = %f  (0.530960)", x, x, rf.evaluate(x)));

		x = 5.0;
		System.out.println(String.format("exp(%f) / 1 + exp(%f) = %f  (0.993307)", x, x, rf.evaluate(x)));

		x = 159.0;
		System.out.println(String.format("exp(%f) / 1 + exp(%f) = %f  (1.000000)", x, x, rf.evaluate(x)));

		System.out.println("\n\n");
	}

}