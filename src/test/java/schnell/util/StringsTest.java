package schnell.util;

import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class Strings_Test {

	@Test
	public void TestrmLastSubstr_normal() {
		System.out.println("\nTestrmLastSubstr_normal");

		StringBuilder sb = new StringBuilder();
		String ss = "\t";

		sb.append("Hello").append(ss).append("world!").append(ss).append("How")
			.append(ss).append("are").append(ss).append("you").append(ss)
			.append("today?");
		String trimeedSb = sb.toString().trim();

		sb.append(ss);
		Strings.rmLastSubstr(sb, ss);
		String rmedSb = sb.toString();

		if (!trimeedSb.equals(rmedSb)) {
			System.out.println("EXPECTING (" + trimeedSb.length() + "): " + trimeedSb);
			System.out.println("ACTUAL:   (" + rmedSb.length() + "): " + rmedSb);
		}
		Assert.assertEquals(true, trimeedSb.equals(rmedSb));
		System.out.println("PASS");
	}

	@Test
	public void TestrmLastSubstr_empty() {
		System.out.println("\nTestrmLastSubstr_empty");

		StringBuilder sb = new StringBuilder();
		String ss = "\t";

		String trimeedSb = sb.toString().trim();

		Strings.rmLastSubstr(sb, ss);
		String rmedSb = sb.toString();

		Assert.assertEquals(true, trimeedSb.equals(rmedSb));
		System.out.println("PASS");
	}

	@Test
	public void nTestrmLastSubstr_single() {
		System.out.println("\nTestrmLastSubstr_single");

		StringBuilder sb = new StringBuilder();
		sb.append("hello");
		String ss = "\t";

		String trimeedSb = sb.toString().trim();

		Strings.rmLastSubstr(sb, ss);
		String rmedSb = sb.toString();

		Assert.assertEquals(true, trimeedSb.equals(rmedSb));
		System.out.println("PASS");
	}

}
