package schnell.process;


/**
 * @param <IN>
 * @param <OUT>
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public interface ConverterExcept<IN, OUT, EXCEPTION extends Exception> {

	/**
	 *
	 */
	public OUT convert(IN input) throws EXCEPTION;

}