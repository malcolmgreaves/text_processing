package schnell.process.text.concurrent;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.maltparser.core.exception.MaltChainedException;
import schnell.data.io.IO;
import schnell.data.write.ProcessedWriter;
import schnell.data.write.Writer;
import schnell.process.text.TextProcessor;
import schnell.run.ReadConfigFile;
import schnell.run.Runner;
import schnell.struct.IdedChunkedParsed;
import schnell.struct.ds.InstanceCreationSource;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com
 */
public abstract class RTextProcessSingle implements Runnable {

  static protected Logger Log = Logger.getLogger(RTextProcessSingle.class);
  // fields
  protected File outputDir;
  protected List<File> inputFiles;
  protected Configuration conf;
  protected TextProcessor textProcessor;
  protected Logger log;

  public RTextProcessSingle(Configuration conf,
                            File outputDir, List<File> inputFiles,
                            Logger log)
      throws MaltChainedException, IOException, ClassNotFoundException {
    Exceptions.checkNonNullNonEmpty(inputFiles, "input files");
    setOutputDir(outputDir);
    this.inputFiles = inputFiles;
    this.conf = conf;
    this.log = log;
    this.textProcessor = TextProcessor.make(conf, log);
  }

  static public void main(String[] args) {
    if (args == null || args.length < 3) {
      throw new IllegalArgumentException(Strings.build("Need at least 3 arguments:\n",
                                                       "configuration file\n",
                                                       "output directory\n",
                                                       "input files or  directories writer/ input files\n"));
    }
    try {
      final File configFi = new File(args[0]), outputDir = new File(args[1]);

      // Below: read configurations and process accordingly

      Log.info("CONFIGURATION FILE:\n\t" + configFi);
      Log.info("OUTPUT WORKING DIRECTORY:\n\t" + outputDir);

      Exceptions.checkFileExists(configFi, "configuration file");

      final List<File> inputFiles = RTextProcess.getInputFiles(2, args);
      Log.info("INPUT FILES (" + inputFiles.size() + "):\n\t" + inputFiles);

      final Configuration conf = ReadConfigFile.produce(configFi);

      final InstanceCreationSource instCreateSrc = InstanceCreationSource.make(conf, Log);

      final String ll = conf.get(ReadConfigFile.LOG_LEVEL, "INFO");

      Log.info(Strings.build("Using these inputs:\n",
                             "instance creation source:             ", instCreateSrc, "\n",
                             "Log level:                            ", ll, "\n",
                             ""
                            ));
      Log.setLevel(Level.toLevel(ll));
      // finished configuration

      // Determine which RTextProcess sub-class to use
      RTextProcessSingle rtextprocess;
      switch (instCreateSrc) {
        case LUCENE:
        case TEXT:
          throw new IllegalArgumentException("unsupported " + instCreateSrc);
        case TEXT_DOCSENTID:
          rtextprocess = new TextDocSentID(conf, outputDir, inputFiles, Log);
          break;
        default:
          throw new IllegalArgumentException(Strings.build(
              "initialization fail from InstanceCreationSrc: ", instCreateSrc));
      }

      // perform text processing :: calls rtextprocess.run()
      System.exit(Runner.run(Log, rtextprocess));

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * The output directory must not exist when calling this method. After calling,
   * this method will have created the output directory & set the internal output
   * directory field to be this directory.
   */
  public void setOutputDir(File outputDir) {
    Exceptions.checkNonNull(outputDir, "output working directory");
    try {
      Exceptions.checkFileDoesntExist(outputDir);
      Exceptions.checkDirDoesntExist(outputDir);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
    if (!outputDir.mkdirs()) {
      throw new IllegalArgumentException("could not create output directory: " + outputDir);
    }
    this.outputDir = outputDir;
  }

  static public class TextDocSentID extends RTextProcessSingle {

    protected Writer<IdedChunkedParsed> outwriter;
    protected int linePeek, maxLineLen;

    public TextDocSentID(Configuration conf, File outputDir, List<File> inputFiles, Logger log)
        throws MaltChainedException, IOException, ClassNotFoundException {
      super(conf, outputDir, inputFiles, log);
      this.outwriter = new ProcessedWriter();
      this.linePeek = conf.getInt(ReadConfigFile.PEEK, 7500);
      log.info("peeking every " + linePeek + " lines");
      this.maxLineLen = conf.getInt(ReadConfigFile.MAX_TOK_SAFE, 500);
      log.info("ignoring lines longer than " + maxLineLen);
    }

    @Override
    public void run() {
      int ii = 0, totalLi = 0;
      for (File fi : inputFiles) {
        log.info("Reading and processing file [" + (++ii) + "/" + inputFiles.size() + "]: " + fi);
        try {
          BufferedReader r = IO.reader(fi);
          BufferedWriter w = IO.writer(new File(outputDir, fi.getName() + ".malt"));
          int li = 1;
          for (String line; (line = r.readLine()) != null; ) {
            if (line.length() > 0 && line.length() < maxLineLen) {
              try {
                process(w, line);
                li++;
              } catch (Exception e) {
                log.error("[skip] error processing line: " + line, e);
              }
              if (li % linePeek == 0) {
                log.info("processed " + li + " lines");
              }
            }
          }
          totalLi += (li - 1);
          w.close();
          r.close();
          log.info("Processed " + totalLi + " so far");
        } catch (IOException e) {
          log.error("[skip] Error processing file: " + fi, e);
        } catch (OutOfMemoryError e) {
          System.gc();
          log.warn("Attempting to recover from out of memory error", e);
        }
      }
      log.info("processed all " + inputFiles.size() +
               " input files, consiting of " + totalLi + " total sentences");
    }

    protected void process(BufferedWriter w, String line) throws Exception {
      String[] bits = line.split("\\t");
      String docsentID = bits[0];
      String text = bits[1];
      IdedChunkedParsed processed = new IdedChunkedParsed(docsentID, textProcessor.process(text));
      outwriter.write(w, processed);
    }
  }

}