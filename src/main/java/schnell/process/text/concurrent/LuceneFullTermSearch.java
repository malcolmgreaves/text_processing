package schnell.process.text.concurrent;

import com.beust.jcommander.internal.Lists;
import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import schnell.data.index.search.LuceneQuerierSelector;
import schnell.data.index.search.Querier;
import schnell.data.io.IO;
import schnell.run.ReadConfigFile;
import schnell.run.Runner;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com
 */
public class LuceneFullTermSearch
        implements Runnable {

    static protected Logger Log = Logger.getLogger(LuceneFullTermSearch.class);
    protected File outputDir;
    protected Configuration conf;

    private LuceneFullTermSearch(File outputDir, Configuration conf) {
        setOutputDir(outputDir);
        this.conf = conf;
    }

    static public void main(String[] args) {
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException(Strings.build("Need at least 2 arguments:\n",
                                                             "configuration file\n",
                                                             "output directory\n"
                                                            ));
        }

        try {
            File configFi = new File(args[0]);
            Log.info("CONFIGURATION FILE:\n\t" + configFi);
            Exceptions.checkFileExists(configFi, "configuration file");
            final Configuration conf = ReadConfigFile.produce(configFi);

            File outputDir = new File(args[1]);
            Log.info("OUTPUT WORKING DIRECTORY:\n\t" + outputDir);

            final String ll = conf.get(ReadConfigFile.LOG_LEVEL, "INFO");

            Log.info(Strings.build("Using these final inputs:\n",
                                   "Log level:                            ", ll, "\n"
                                  ));
            Log.setLevel(Level.toLevel(ll));

            System.exit(Runner.run(Log, new LuceneFullTermSearch(outputDir, conf)));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static public Iterator<Document> allDocsOfTerm(Configuration conf, Logger log)
            throws IOException {
        LuceneQuerierSelector lqs = LuceneQuerierSelector.make(conf, Log);
        String term = conf.get("TERM");
        final List<Iterator<Document>> iters = Lists.newArrayList(
                lqs.select(Querier.Type.NEWSWIRE).iterateOverAllDocs(),
                lqs.select(Querier.Type.DISCUSSION).iterateOverAllDocs(),
                lqs.select(Querier.Type.WEB).iterateOverAllDocs()
                                                                 );

        return new Iterator<Document>() {

            private int selector = 0;

            @Override
            public boolean hasNext() {
                for (; selector < iters.size(); selector++) {
                    if (iters.get(selector).hasNext()) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public Document next() {
                return iters.get(selector).next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("remove on a multi all-document iterator");
            }
        };
    }

    /**
     * The output directory must not exist when calling this method. After calling,
     * this method will have created the output directory & set the internal output
     * directory field to be this directory.
     */
    public void setOutputDir(File outputDir) {
        Exceptions.checkNonNull(outputDir, "output working directory");
        try {
            Exceptions.checkFileDoesntExist(outputDir);
            Exceptions.checkDirDoesntExist(outputDir);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        if (!outputDir.mkdirs()) {
            throw new IllegalArgumentException("could not create output directory: " + outputDir);
        }
        this.outputDir = outputDir;
    }

    @Override
    public void run() {
        try {
            BufferedWriter w = IO.writer(new File(outputDir, "all_terms.bz2"));

            // iterate through documents, load-balance equally and submit to processor threads
            Document doc;
            Iterator<Document> documentIterator = allDocsOfTerm(conf, Log);

            int di = 0;
            for (; documentIterator.hasNext(); di++) {
                try {
                    doc = documentIterator.next();
                    w.write(doc.get("docsentid"));
                    w.write("\t");
                    w.write(doc.get("contents"));
                    w.newLine();

                } catch (IOException e) {
                    Log.error(Strings.build("[skip] couldn't write document # ", di + 1), e);
                }
                if (di % 100000 == 0) {
                    Log.info(Strings.build("submitted ", di, " documents"));
                }
            }
            Log.info(Strings.build("total: submitted ", di, " documents"));

            w.close();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}