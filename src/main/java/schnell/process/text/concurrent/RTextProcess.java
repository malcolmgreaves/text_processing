package schnell.process.text.concurrent;

import com.beust.jcommander.internal.Maps;
import com.google.common.collect.Lists;
import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import schnell.concurrent.ProcessorThread;
import schnell.concurrent.PutChanTerminableThread;
import schnell.data.index.search.KBPQuery;
import schnell.data.index.search.LuceneQuerierSelector;
import schnell.data.io.IO;
import schnell.data.read.QueryParser;
import schnell.data.write.ProcessedWriter;
import schnell.data.write.Writer;
import schnell.pipeline.QuerySetup;
import schnell.pipeline.test.*;
import schnell.run.ReadConfigFile;
import schnell.run.Runner;
import schnell.struct.IdedChunkedParsed;
import schnell.struct.doc.ScoredDocument;
import schnell.struct.ds.InstanceCreationSource;
import schnell.struct.ds.PutChannel;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.*;
import java.util.*;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com
 */
public abstract class RTextProcess
    extends ConcurrentMakeSlotfillInstances<ProcessedQuery>
    implements Runnable {

  static protected Logger Log = Logger.getLogger(RTextProcess.class);
  protected File outputDir;
  protected List<File> inputFiles;
  protected Configuration conf;

  public RTextProcess(int nThreads, Configuration conf,
                      File outputDir, List<File> inputFiles) {
    super(nThreads);
    Exceptions.checkNonNullNonEmpty(inputFiles, "input files");
    setOutputDir(outputDir);
    this.inputFiles = inputFiles;
    this.conf = conf;
  }

  static public List<File> getInputFiles(int startFrom, String[] args) {
    Exceptions.checkIsNonNegative(startFrom, "starting from");
    Exceptions.checkNonNullNonEmpty(args, "arguments");
    Exceptions.checkIsNonNegative(args.length - startFrom, "range");

    File fi;
    List<File> inputFiles = Lists.newArrayList();
    for (int ii = startFrom; ii < args.length; ii++) {
      fi = new File(args[ii]);
      if (fi.isDirectory()) {
        for (File inside : fi.listFiles()) {
          if (inside.isFile()) {
            inputFiles.add(inside);
          }
        }
      } else if (fi.isFile()) {
        inputFiles.add(fi);
      }
    }
    return inputFiles;
  }

  static public Map<String, List<String>> loadAnswers(String answersFi)
      throws IOException {
    Map<String, List<String>> answers = Maps.newHashMap();

    BufferedReader r = IO.reader(answersFi);
    for (String line; (line = r.readLine()) != null; ) {
      final String[] bits = Strings.tabSplit(line);
      final String queryName = bits[0];
      final List<String> searchTerms = Lists.newArrayListWithCapacity(bits.length - 2);
      answers.put(queryName, searchTerms);
      // skip over the query name, which is the 2nd bit (bits[1])
      searchTerms.addAll(Arrays.asList(bits).subList(2, bits.length));
    }
    r.close();
    return answers;
  }

  static public void main(String[] args) {
    if (args == null || args.length < 3) {
      throw new IllegalArgumentException(Strings.build("Need at least 3 arguments:\n",
                                                       "configuration file\n",
                                                       "output directory\n",
                                                       "input files or  directories writer/ input files\n"));
    }
    try {
      final File configFi = new File(args[0]), outputDir = new File(args[1]);

      // Below: read configurations and process accordingly

      Log.info("CONFIGURATION FILE:\n\t" + configFi);
      Log.info("OUTPUT WORKING DIRECTORY:\n\t" + outputDir);

      Exceptions.checkFileExists(configFi, "configuration file");

      final List<File> inputFiles = getInputFiles(2, args);
      Log.info("INPUT FILES (" + inputFiles.size() + "):\n\t" + inputFiles);

      final Configuration conf = ReadConfigFile.produce(configFi);

      int nThreads = conf.getInt(ReadConfigFile.N_THREADS, 4);
      if (nThreads < 1) {
        nThreads = 1;
      }

      final InstanceCreationSource instCreateSrc = InstanceCreationSource.make(conf, Log);

      final String ll = conf.get(ReadConfigFile.LOG_LEVEL, "INFO");

      Log.info(Strings.build("Using these inputs:\n",
                             "# threads:                            ", nThreads, "\n",
                             "instance creation source:             ", instCreateSrc, "\n",
                             "Log level:                            ", ll, "\n",
                             ""
                            ));
      Log.setLevel(Level.toLevel(ll));
      // finished configuration

      // Determine which RTextProcess sub-class to use
      RTextProcess rtextprocess;
      switch (instCreateSrc) {
        case LUCENE:
          rtextprocess = Lucene.make(nThreads, conf, outputDir, inputFiles.get(0));
          break;
        case TEXT:
          rtextprocess = new Text(nThreads, conf, outputDir, inputFiles);
          break;
        case TEXT_DOCSENTID:
          rtextprocess = new TextDocSentID(nThreads, conf, outputDir, inputFiles);
          break;
        default:
          throw new IllegalArgumentException(Strings.build(
              "initialization fail from InstanceCreationSrc: ", instCreateSrc));
      }

      // perform text processing :: calls rtextprocess.run()
      System.exit(Runner.run(Log, rtextprocess));

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * The output directory must not exist when calling this method. After calling,
   * this method will have created the output directory & set the internal output
   * directory field to be this directory.
   */
  public void setOutputDir(File outputDir) {
    Exceptions.checkNonNull(outputDir, "output working directory");
    try {
      Exceptions.checkFileDoesntExist(outputDir);
      Exceptions.checkDirDoesntExist(outputDir);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
    if (!outputDir.mkdirs()) {
      throw new IllegalArgumentException("could not create output directory: " + outputDir);
    }
    this.outputDir = outputDir;
  }

  @Override
  public Writer<ProcessedQuery> outputWriter() {
    final Writer<IdedChunkedParsed> processedWriter = new ProcessedWriter();
    return new Writer<ProcessedQuery>() {
      @Override
      public void write(final BufferedWriter w, final ProcessedQuery input) throws IOException {
        for (IdedChunkedParsed processedSentence : input.processedSentences) {
          processedWriter.write(w, processedSentence);
        }
      }
    };
  }

  @Override
  public BufferedWriter outputBufferedWriter(final String name)
      throws IOException {
    Exceptions.checkNonNullNonEmpty(name, "name");
    return IO.writer(new File(outputDir, name));
  }

  // The returned ProcessorThread
  @Override
  protected PutChanTerminableThread<DocumentsQuery, ProcessedQuery> threadMaker(PutChannel<ProcessedQuery> c) {
    try {
      return new ProcessorThread(1, conf, c);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  // Lucene treats input file as a sequence of queries. The queries are in
  // TAC-KBP 2012 xml format.
  static public class Lucene extends RTextProcess {

    final private KBPQueriesToDocuments queries2extractables;

    protected Lucene(int nThreads, Configuration conf, File outputDir, File kbpQueries,
                     KBPQueriesToDocuments queries2extractables) {
      super(nThreads, conf, outputDir, Lists.newArrayList(kbpQueries));
      this.queries2extractables = queries2extractables;
    }

    static public Lucene make(int nThreads, Configuration conf, File outputDir, File kbpQueries
                             ) throws IllegalArgumentException, IOException {

      QuerySetup qs = QuerySetup.make(conf, Log);
      switch (qs.qt) {
        case QNAME_AND_ANSWER:
          Log.info(Strings.build("loading answers from: ", conf.get(ReadConfigFile.ANSWERS)));
          return new Lucene(nThreads, conf, outputDir, kbpQueries,
                            new KBPQueriesToDocuments_WithAns(
                                Log,
                                LuceneQuerierSelector.make(conf, Log),
                                qs,
                                loadAnswers(conf.get(ReadConfigFile.ANSWERS))
                            )
          );

        case FIND_DOCSENTID:
          Log.info("querying for text, finding document IDs, then grabbing all sentences in the found documents");
          return new Lucene(nThreads, conf, outputDir, kbpQueries,
                            new KBPQueriesToDocuments_FindDocSentID(
                                Log,
                                LuceneQuerierSelector.make(conf, Log),
                                qs));

        default:
          Log.info("searching using query text");
          return new Lucene(nThreads, conf, outputDir, kbpQueries,
                            new KBPQueriesToDocuments(
                                Log,
                                LuceneQuerierSelector.make(conf, Log),
                                qs
                            )
          );
      }
    }

    @Override
    public Iterator<DocumentsQuery> getDocumentsByQuery() {
      try {
        // get the queries
        final List<KBPQuery> official_queries = Lists.newArrayList();
        for (File inFi : inputFiles) {
          if (Log.isDebugEnabled()) {
            Log.debug("using " + inFi + " as KBP queries input file");
          }
          official_queries.addAll(QueryParser.parseXML(inFi));
        }
        Log.info("# official queries:   " + official_queries.size());

        return new Iterator<DocumentsQuery>() {

          private int index = 0;
          private DocumentsQuery next = null;

          @Override
          public boolean hasNext() {
            if (index >= official_queries.size()) {
              return false;
            }
            while (index < official_queries.size()) {
              try {
                KBPQuery query = official_queries.get(index++);
                next = new DocumentsQuery(
                    query.queryName,
                    // performs qs3://cmuwcohen0/data/kbp/text/divided_sanitized_32_part-9/uery using lucene
                    queries2extractables.convert(query)
                );
                break;

              } catch (Exception e) {
                Log.error(Strings.build("Error trying to convert for query # ", index, ":\n",
                                        official_queries.get(index - 1)
                                                        .toString(), "\nSkipping query"), e);
              }
            }
            return next != null;
          }

          @Override
          public DocumentsQuery next() {
            return next;
          }

          @Override
          public void remove() {
            throw new UnsupportedOperationException("will not remove from official queries");
          }
        };
      } catch (DocumentException e) {
        throw new IllegalStateException(e);
      }
    }
  }

  // Text treats each input file as a set of '\n' delimited strings.
  // Each string is a sentence's text. The entire file is considered a distinct
  // document. Sentences are assumed to be in-order. The document-sentence ID
  // for a sentence is the file name and the sentence line. e.g. the docsentID
  // of the 12th sentence in a file named "bob's_your_uncle" would be
  // bob's_your_uncle.S12
  static public class Text extends RTextProcess {
    public Text(int nThreads, Configuration conf, File outputDir, List<File> inputFiles) {
      super(nThreads, conf, outputDir, inputFiles);
    }

    @Override
    public Iterator<DocumentsQuery> getDocumentsByQuery() {
      // read in text sentences, one per line, as an iterator
      Log.info("Reading sentences from disk");

      final File[] inputFis = inputFiles.toArray(new File[inputFiles.size()]);

      // custom iterator
      return new Iterator<DocumentsQuery>() {

        private int fiIndex = 0;
        private DocumentsQuery next;
        private FileContext fc = getNextFile();

        private FileContext getNextFile() {
          try {
            // get the next input file, if there is one that we haven't read from yet
            while (fiIndex < inputFis.length) {
              File fi = inputFis[fiIndex++];
              if (fi.isFile()) {
                Log.info(Strings.build("Reading file [ ", fiIndex, " / ",
                                       inputFis.length, " ]: ", fi));
                return new FileContext(fi, 5000);
              }
            }
          } catch (Exception e) {
            Log.error("Error trying to open reader to file: " + inputFis[fiIndex - 1], e);
          }
          return null;
        }

        @Override
        public boolean hasNext() {
          if (fc == null) {
            return false;
          }
          try {
            next = loadFrom(fc);
            if (next == null) {
              fc = getNextFile();
              return hasNext();
            }
            return true;
          } catch (IOException e) {
            Log.error("Error reading from file: " + fc.fi + "  moving on", e);
            fc = getNextFile();
            return hasNext();
          }
        }

        @Override
        public DocumentsQuery next() {
          return next;
        }

        @Override
        public void remove() {
          throw new UnsupportedOperationException("will not remove from files");
        }
      };
    }

    /**
     * @param fc the file context to read from
     * @return the documens query that was read, or null if there was nothing read
     * @throws IOException
     */
    protected DocumentsQuery loadFrom(FileContext fc) throws IOException {
      final String name = fc.fi.getName();
      final DocumentsQuery dq = new DocumentsQuery(name, new ArrayList<ScoredDocument>(300000));
      int si = 0;
      for (String line; (line = fc.r.readLine()) != null && si < fc.grabLimit; si++) {
        try {
          line = line.trim();
          if (line.length() > 0) {
            storeline(dq, name, si, line);
          }
        } catch (Exception e) {
          Log.error("error processing line # " + si + "\n" + line, e);
        }
      }
      if (dq.documents.size() == 0) {
        fc.r.close();
        return null;
      }
      return dq;
    }

    protected void storeline(final DocumentsQuery dq, final String name, final int si, final String line) {
      dq.documents.add(new ScoredDocument(
          Strings.build(name, ".S", si),
          1.0,
          line));
    }

    static class FileContext {
      final public File fi;
      final public BufferedReader r;
      final public int grabLimit;

      public FileContext(File fi, int grabLimit) throws FileNotFoundException {
        this.fi = fi;
        this.r = new BufferedReader(new FileReader(this.fi));
        this.grabLimit = grabLimit;
      }
    }
  }

  // Same function as the Text subclass. However, TextDocSentID interprets lines as a pair of values:
// (document ID and sentence number , sentence text)
// which are separated by a "\t" character.
  static public class TextDocSentID extends Text {
    public TextDocSentID(int nThreads, Configuration conf, File outputDir, List<File> inputFiles) {
      super(nThreads, conf, outputDir, inputFiles);
    }

    @Override
    protected void storeline(final DocumentsQuery dq, final String n,
                             final int si, final String line) {
      String[] bits = Strings.tabSplit(line);
      dq.documents.add(new ScoredDocument(
          bits[0],  // docsentID
          1.0,
          bits[1])); // actual text
    }
  }


}