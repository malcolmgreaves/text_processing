package schnell.process.text.tag;

import java.util.List;

import schnell.struct.text.tag.POSTagged;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface POSTagger {
  
  /**
   * 
   * @param documents
   * @return
   */
  public List<POSTagged> tag(String text);
  
}