package schnell.process.text.tag;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import schnell.struct.text.tag.POSTagged;
import schnell.util.Strings;
import schnell.util.except.Exceptions;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class StanfordPOSTagger
implements POSTagger {
  /* methods */
  
  @Override
  public List<POSTagged> tag(String text) {
    // peform POS tagging
    String[] tStrWords = Strings.spaceSplit(tagger.tagString(text).trim());

    // convert parsed POS sentence string into a list of tagged word s
    List<POSTagged> taggedWords = new ArrayList<POSTagged>(tStrWords.length);
    TaggedWord tword;
    for(int ii = 0; ii < tStrWords.length; ii++){
      tword = new TaggedWord();
      tword.setFromString(tStrWords[ii], "_");
      taggedWords.add(new POSTagged(tword.word(), tword.tag()));
    }
    return taggedWords;
  }


  /* constructors */
  
  public StanfordPOSTagger(){
    this(new MaxentTagger());
  }
  
  public StanfordPOSTagger(MaxentTagger posTagger){
    setTagger(posTagger);
  }
  
  static public StanfordPOSTagger make()
      throws IOException{
    return make(new File(StanfordPOSTagger.DEFAULT_LOCATION));
  }
  
  static public StanfordPOSTagger make(File taggingModelFi)
      throws IOException{
    return new StanfordPOSTagger(new MaxentTagger(taggingModelFi.getCanonicalPath()));
  }

  /* fields */

  protected MaxentTagger tagger;
  public MaxentTagger tagger(){ return tagger; }
  public void setTagger(MaxentTagger tagger){
    Exceptions.checkNonNull(tagger, "maxent pos tagger");
    this.tagger = tagger;
  }

  static public final String
  DEFAULT_LOCATION = "models/stanford_pos_tagger/wsj-0-18-bidirectional-nodistsim.tagger";

}