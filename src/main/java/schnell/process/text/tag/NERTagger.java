package schnell.process.text.tag;

import java.util.List;

import schnell.struct.text.tag.NERTagged;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface NERTagger {

  public List<NERTagged> nerTag(String text);
  
}