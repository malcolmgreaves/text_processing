package schnell.process.text.tag;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import schnell.struct.text.tag.NERTagged;
import schnell.util.except.Exceptions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class StanfordNERTagger
        implements NERTagger {

    static final public String
            DEFAULT_NER_MODEL_LOC = "models/stanford_ner_tagger/english.all.3class.distsim.crf.ser.gz";

    /* constructors */
    protected AbstractSequenceClassifier<CoreLabel> classifier;

  /* class fields */

    public StanfordNERTagger(AbstractSequenceClassifier<CoreLabel> nerClassifier) {
        setNERClassifier(nerClassifier);
    }

    static public StanfordNERTagger make(File nerClassifierLoc)
            throws IOException, ClassNotFoundException {
        Exceptions.checkFileExists(nerClassifierLoc, "NER classifier location");

        return new StanfordNERTagger(
                CRFClassifier.getClassifier(nerClassifierLoc.getCanonicalPath()));
    }

    static public StanfordNERTagger make()
            throws IOException, ClassNotFoundException {
        return make(new File(DEFAULT_NER_MODEL_LOC));
    }

    /* class methods */
    @Override
    public List<NERTagged> nerTag(String text) {
        Exceptions.checkNonNull(text);

        List<List<CoreLabel>> classified = classifier.classify(text);
        List<NERTagged> tagged = new ArrayList<NERTagged>(classified.size());
        for (List<CoreLabel> c : classified) {
            for (CoreLabel word : c) {
                tagged.add(new NERTagged(word.word(), word.get(CoreAnnotations.AnswerAnnotation.class)));
            }
        }
        return tagged;
    }

    public AbstractSequenceClassifier<CoreLabel> nerClassifier() { return classifier; }

    public void setNERClassifier(AbstractSequenceClassifier<CoreLabel> nerClassifier) {
        Exceptions.checkNonNull(nerClassifier, "NER classifier");
        this.classifier = nerClassifier;
    }

}