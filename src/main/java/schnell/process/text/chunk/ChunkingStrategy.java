package schnell.process.text.chunk;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public enum ChunkingStrategy {
  NN,
  NER,
  NER_THEN_NN,
  DPMOD,
  NER_THEN_DPMOD,
  IDENTITY;
  
  public PhraseChunker make(){
    switch(this){
    case NN:
      return new NNPhraseChunker(); 
      
    case NER:
      return new NERPhraseChunker();
      
    case NER_THEN_NN:
      return new NERandNNPhraseChunker();
      
    case DPMOD:
      return new DPModPhraseChunker();
      
    case NER_THEN_DPMOD:
      return new NERandDPModPhraseChunker();
      
    case IDENTITY:
      return PhraseChunker.IDENTITY; 
    }
    throw new IllegalStateException("invalid chunking strategy: "+this);
  }
  
}