package schnell.process.text.chunk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.TextPosNerOffset;
import schnell.struct.text.tok.TextPosNerDpOffsetRecord;
import schnell.struct.text.tok.TokenPosNerDpOffset;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 * @author Kathryn Mazaitis
 * 
 */
public class NERPhraseChunker
implements PhraseChunker {
	static protected Logger Log = Logger.getLogger(NERPhraseChunker.class);
	static {
		Log.setLevel(Level.ERROR);
	}
	/* methods */

	/**
	 * Same as "chunk()". Since the NER chunking strategy doesn't use dependency
	 * information, the minimum necessary input doesn't need this information
	 * either. This method is a wrapper: it accesses chunk(). The returned tokens
	 * are chunked, but have no valid dependency information.
	 * 
	 * @param tokens
	 * @return
	 */
	public List<TextPosNerDpOffset> chunkTagged(List<TextPosNerOffset> tokens){
		Exceptions.checkNonNull(tokens, "tokens to chunk");

		List<TextPosNerDpOffset> tokensCopy = new ArrayList<TextPosNerDpOffset>(tokens.size());
		for(TextPosNerOffset tok : tokens){
			tokensCopy.add(new TokenPosNerDpOffset(tok));
		}
		return chunk(tokensCopy);
	}

	/**
	 * 
	 */
	@Override
	public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens){
		Exceptions.checkNonNull(tokens, "tokens to chunk");

		int nTokens = tokens.size();
		PhraseChunkNode curr = null;

		// build the token-PhraseChunkNode list
		List<PhraseChunkNode> nodes = new ArrayList<PhraseChunkNode>();
		for(int ii = 0; ii < nTokens; ii++){
			nodes.add(new PhraseChunkNode(ii+1, new TokenPosNerDpOffset(tokens.get(ii))));
		}

		// set the depends reference for every token-PhraseChunkNode
		// and build an inverted index for the dependencies on each PhraseChunkNode
		// and stage the merging of all sequences of token nodes with the
		// same NER tags
		Map<PhraseChunkNode, List<PhraseChunkNode>> node2dependsOn = Maps.newHashMap();

		BufferChunk bc = new BufferChunk(nodes, new ArrayList<PhraseChunkNode>(), node2dependsOn);

		String currChunkingNERTag = null;

		for(int ii = 0; ii < nTokens; ii++){
			curr = nodes.get(ii);
			// Set the depends reference for the currently observed PhraseChunkNode.
			try{
				curr.depends = curr.tok.dependencyIndex() > 0 ? nodes.get(curr.tok.dependencyIndex()-1) : null;
			} catch(Exception e){
				String s = "INDEX EXCEPTION PROCESSING TOKEN:    "+curr+"\n"+
						"All tokens:                    \n"+tokens+"\n";
				throw new UnsupportedOperationException(s+"\n\nORIGINAL EXPCETION:\n"+e.toString());
			}
			if(currChunkingNERTag == null){
				currChunkingNERTag = curr.tok.nerTag();
			}

			if(currChunkingNERTag != null
					&& !curr.tok.nerTag().equals(currChunkingNERTag)){
				if(bc.buffer.size() > 0){
					try{
						// flush the buffer and do merging
						bc.mergeBuffer();
					}catch(IllegalStateException e){
						Log.warn(Strings.build("failed to merge a buffer, tokens:\n",
								new TextPosNerDpOffsetRecord(tokens).toString(),"\nError:\n",
								Exceptions.toString(e)));
					}
				}
				currChunkingNERTag = null;
			}

			if(!curr.tok.nerTag().equals("O")){
				// Stage the merging of PhraseChunkNode we're currently observing into
				// our PhraseChunkNode buffer
				bc.buffer.add(curr);
				currChunkingNERTag = curr.tok.nerTag();
			}
		}
		// final buffer merge
		if(bc.buffer.size() > 0){
			try{
				// flush the buffer and do merging
				bc.mergeBuffer();
			}catch(IllegalStateException e){
				Log.warn(Strings.build("failed to merge a buffer, tokens:\n",
						new TextPosNerDpOffsetRecord(tokens).toString(),"\nError:\n",
						Exceptions.toString(e)));
			}
		}

		// complete merges and fix all dependency errors from the merge operation
		for(int ii = 0; ii < nTokens; ii++){
			curr = nodes.get(ii);
			if(curr.toMerge.size() > 0){
				// merge all nodes in toMerge with the current PhraseChunkNode
				List<PhraseChunkNode> merged = new ArrayList<PhraseChunkNode>();
				merged.addAll(curr.toMerge);
				curr.merge();

				// fix dependency errors from merge
				for(PhraseChunkNode m : merged){
					// to indicate that the PhraseChunkNode was merged, set it's index to -1
					m.index = -1;
					if(node2dependsOn.containsKey(m)){
						for(PhraseChunkNode dependedOnM : node2dependsOn.get(m)){
							// makeBloomFilter the PhraseChunkNode that depended on m now depend on the PhraseChunkNode that
							// m merged into 
							dependedOnM.depends = curr;
						}
					}
				}
			}
		}

		// re-index the merged list
		int newIndex = 1;
		for(int ii = 0; ii < nodes.size(); ii++){
			curr = nodes.get(ii);
			if(curr.index == -1){
				nodes.remove(ii);
				ii--;
			} else {
				curr.index = newIndex;
				newIndex++;
			}
		}

		// fix depends head index to match merged state
		// and makeBloomFilter final chunked hazy token list
		List<TextPosNerDpOffset> chunked = Lists.newArrayList();
		int nMergedTokens = nodes.size();
		for(int ii = 0; ii < nMergedTokens; ii++){
			curr = nodes.get(ii);
			if(curr.tok.dependencyIndex() > 0){
				curr.tok.setDependencyIndex(curr.depends.index); 
			}
			chunked.add(curr.tok);
		}

		return chunked;
	}



	/* internal static classes to assist with methods */

	class BufferChunk {
		/* methods */

		public String toString(){
			return Strings.build("buffer: ",buffer);
		}

		public void mergeBuffer()
				throws IllegalStateException {

			// get the buffer PhraseChunkNode's indices
			int[] bufNodeIndices = new int[buffer.size()];
			for(int bi = 0; bi < bufNodeIndices.length; bi++){
				bufNodeIndices[bi] = buffer.get(bi).index;
			}

			// Stage all merges for all nodes in the buffer to be merged into
			// the single head PhraseChunkNode of the buffer.
			// If no such single head PhraseChunkNode exists, or if there is more than one,
			// then throw an IllegalStateException.
			int dependsOn = SINGLE_HEAD_INDEX_INIT, singleHeadIndex = SINGLE_HEAD_INDEX_INIT,
					headDependsOn = SINGLE_HEAD_INDEX_INIT;
			for(int bi = 0; bi < bufNodeIndices.length; bi++){

				dependsOn = Arrays.binarySearch(bufNodeIndices, buffer.get(bi).tok.dependencyIndex());

				if(dependsOn < 0){
					if(buffer.get(bi).tok.dependencyIndex() == headDependsOn
							|| singleHeadIndex == SINGLE_HEAD_INDEX_INIT){
						singleHeadIndex = bi;
						headDependsOn = buffer.get(bi).tok.dependencyIndex();

					} else {
						String bufStr = toString();
						buffer.clear();
						throw new IllegalStateException(Strings.build(
								"buffer doesn't have a single, unambigious head! buffer: ",
								bufStr));
					}
				}
			}
			if(singleHeadIndex < 0){
				String bufStr = toString();
				buffer.clear();
				throw new IllegalStateException(Strings.build("buffer doesn't have a head: ",
						bufStr));
			}

			for(int bi = 0; bi < bufNodeIndices.length; bi++){
				if(bi != singleHeadIndex){
					buffer.get(singleHeadIndex).toMerge.add(buffer.get(bi));              
				}
			}

			// find all nodes that depend on any PhraseChunkNode within the buffer
			List<PhraseChunkNode> dependsOnBuffer = new ArrayList<PhraseChunkNode>();
			int nTokens = nodes.size();
			for(int jj = 0; jj < nTokens; jj++){
				PhraseChunkNode depender = nodes.get(jj);
				dependsOn = Arrays.binarySearch(bufNodeIndices, depender.tok.dependencyIndex());
				if(dependsOn >= 0){
					// Remember that the PhraseChunkNode depender depends on the PhraseChunkNode that we're
					// currently observing. We will later have to modify these
					// dependency relationships since we're going to merge the
					// current PhraseChunkNode into the PhraseChunkNode that it depends on.
					dependsOnBuffer.add(depender);
				}
			}

			if(dependsOnBuffer.size() > 0){
				node2dependsOn.put(buffer.get(singleHeadIndex), dependsOnBuffer);
			}

			// clear buffer
			buffer.clear();
		}

		/* constructors */

		public BufferChunk(List<PhraseChunkNode> nodes, List<PhraseChunkNode> buffer,
				Map<PhraseChunkNode, List<PhraseChunkNode>> node2dependsOn){
			this.nodes = nodes;
			this.buffer = buffer;
			this.node2dependsOn = node2dependsOn;
		}

		/* fields */

		List<PhraseChunkNode> nodes, buffer;
		Map<PhraseChunkNode, List<PhraseChunkNode>> node2dependsOn;
	}

	/* constructors */

	public NERPhraseChunker(){}


	/* static members */
	static final public int SINGLE_HEAD_INDEX_INIT = -9;

}
