package schnell.process.text.chunk;

import java.util.List;

import org.apache.log4j.Logger;

import schnell.struct.text.TextPosNerDpOffset;

/**
 * Does NER chunking. Then, on the subsequent output, does NN chunking.
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class NERandDPModPhraseChunker
implements PhraseChunker {
  static protected Logger Log = Logger.getLogger(NERandDPModPhraseChunker.class);
  
  /* methods */

  /**
   * Does NER then NN chunking.
   */
  @Override
  public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens) 
      throws IllegalStateException {
    return dpmod.chunk(ner.chunk(tokens));
  }
  
  /* constructors */

  public NERandDPModPhraseChunker(){
    this.ner = new NERPhraseChunker();
    this.dpmod = new DPModPhraseChunker();
  }
  
  /* fields */

  protected PhraseChunker ner, dpmod;
  
}