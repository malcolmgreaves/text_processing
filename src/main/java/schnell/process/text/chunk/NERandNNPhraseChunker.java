package schnell.process.text.chunk;

import java.util.List;

import org.apache.log4j.Logger;

import schnell.struct.text.TextPosNerDpOffset;

/**
 * Does NER chunking. Then, on the subsequent output, does NN chunking.
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 * @author Kathryn Mazaitis
 */
public class NERandNNPhraseChunker
implements PhraseChunker {
  static protected Logger Log = Logger.getLogger(NERandNNPhraseChunker.class);
  
  /* methods */

  /**
   * Does NER then NN chunking.
   */
  @Override
  public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens) 
      throws IllegalArgumentException {
    List<TextPosNerDpOffset> chunked = null;
    // NER chunking
    try{
      chunked = ner.chunk(tokens);
    }catch(Exception e){
      Log.warn("failed to NER chunk tokens at all");
    }
    // NN chunking
    try{
      chunked = nn.chunk(chunked == null ? tokens : chunked);
    }catch(Exception e){
      Log.warn("failed to NN chunk tokens at all");
    }
    if(chunked != null){
      return chunked;  
    }
    throw new IllegalArgumentException("could not chunk tokens at all");
  }
  
  /* constructors */

  public NERandNNPhraseChunker(){
    this.ner = new NERPhraseChunker();
    this.nn = new NNPhraseChunker();
  }
  
  /* fields */

  protected PhraseChunker ner, nn;
  
}