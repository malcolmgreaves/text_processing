package schnell.process.text.chunk;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import schnell.struct.text.TextPosNerDpOffset;

import com.google.common.collect.Lists;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class PhraseChunkNode {
	/* methods */

	public void merge(){
		if(toMerge.size() > 0){
			toMerge.add(this);
			Collections.sort(toMerge, new Comparator<PhraseChunkNode>(){
				@Override public int compare(PhraseChunkNode o1, PhraseChunkNode o2) {
					return o1.index - o2.index;
				}});
			
			StringBuilder sb = new StringBuilder();
			for(PhraseChunkNode n : toMerge){
				sb.append(n.tok.text()).append(" ");
			}
			tok.setText(sb.toString().trim());

			// set this merged token's beginning offset to the first toMerge token's
			// beginning offset
			tok.setBeg(toMerge.get(0).tok.beg());
			// also set the end offset, makeBloomFilter sure to include the # of spaces added
			// during the merge
			tok.setEnd(toMerge.get(toMerge.size()-1).tok.end());
			
			toMerge = Lists.newArrayList();
		}
	}

	public String toString(){
		return new StringBuilder()
		.append(tok==null ? "null" : tok.toString())
		.append("(")
		.append(index)
		.append(")")
		.toString();
	}

	/* constructors */

	public PhraseChunkNode(){ 
		this(-1, null);
	}

	public PhraseChunkNode(int index, TextPosNerDpOffset tok){
		this.index = index;
		this.tok = tok;
		this.toMerge = Lists.newArrayList();
		depends = null;
		next = null;
		prev = null;
	}

	/* fields */

	public TextPosNerDpOffset tok;
	public List<PhraseChunkNode> toMerge;
	public PhraseChunkNode depends, next, prev;
	public int index;

}