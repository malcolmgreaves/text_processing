package schnell.process.text.chunk;

import java.util.List;

import schnell.struct.text.TextPosNerDpOffset;

/**
 * v1.0: DepParseChunker chunks noun modifiers with their respective nouns. 
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 * 
 */
public interface PhraseChunker {

  /**
   * Chunk copys the input token list, performs a maximal noun phrase
   * chunking on the copy, and returns the copy.
   * @param tokens unchunked data 
   * @return chunked data
   */
  public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens);

  
  /**
   * Does no chunking; returns its input.
   */
  static final public PhraseChunker IDENTITY = new PhraseChunker(){
    /**
     * Does no chunking; returns its input.
     */
    @Override
    public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens) { return tokens; }
  };
  
}