package schnell.process.text.chunk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.tok.TokenPosNerDpOffset;
import schnell.util.except.Exceptions;

/**
 * v1.0: DepParseChunker chunks noun modifiers with their respective nouns. 
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 * 
 */
public class NNPhraseChunker
implements PhraseChunker {

  public NNPhraseChunker(){}

  /**
   * Chunk copys the input token list, performs a maximal noun phrase
   * chunking on the copy, and returns the copy.
   * @param tokens unchunked data 
   * @return chunked data
   */
  @Override
  public List<TextPosNerDpOffset> chunk(List<TextPosNerDpOffset> tokens){
    Exceptions.checkNonNull(tokens, "tokens to chunk");
    
    int nTokens = tokens.size();
    PhraseChunkNode curr = null;

    // build the token-node list
    List<PhraseChunkNode> nodes = new ArrayList<PhraseChunkNode>();
    for(int ii = 0; ii < nTokens; ii++){
      nodes.add(new PhraseChunkNode(ii+1, new TokenPosNerDpOffset(tokens.get(ii))));
    }

    // set the depends reference for every token-node
    // and build an inverted index for the dependencies on each node
    // and stage the merging of all token nodes with "nn" dependency labels
    Map<PhraseChunkNode, List<PhraseChunkNode>> node2dependsOn = new HashMap<PhraseChunkNode,List<PhraseChunkNode>>();
    for(int ii = 0; ii < nTokens; ii++){
      curr = nodes.get(ii);
      if(curr.tok.dependencyIndex() > 0){
        // Set the depends reference for the currently observed node.
        try{
          curr.depends = nodes.get(curr.tok.dependencyIndex()-1);
        } catch(Exception e){
          String s = "INDEX EXCEPTION PROCESSING TOKEN:    "+curr+"\n"+
              "All tokens:                    \n"+tokens+"\n";
          throw new UnsupportedOperationException(s+"\n\nORIGINAL EXPCETION:\n"+e.toString());
        }
        if(curr.tok.depParseLabel().equals("nn")){
          // Stage the merging of node we're currently observing into the node
          // that the current node depends on with a "nn" dependency.
          curr.depends.toMerge.add(curr);

          List<PhraseChunkNode> dependsOnCurr = new ArrayList<PhraseChunkNode>();
          for(int jj = 0; jj < nTokens; jj++){
            PhraseChunkNode depender = nodes.get(jj);
            if(depender.tok.dependencyIndex() == curr.index){
              // Remember that the node depender depends on the node that we're
              // currently observing. We will later have to modify these
              // dependency relationships since we're going to merge the
              // current node into the node that it depends on.
              dependsOnCurr.add(depender);
            }
          }
          if(dependsOnCurr.size() > 0){
            node2dependsOn.put(curr, dependsOnCurr);
          }
        }
      }
    }

    // complete merges and fix all dependency errors from the merge operation
    for(int ii = 0; ii < nTokens; ii++){
      curr = nodes.get(ii);
      if(curr.toMerge.size() > 0){
        // merge all nodes in toMerge with the current node
        List<PhraseChunkNode> merged = new ArrayList<PhraseChunkNode>();
        merged.addAll(curr.toMerge);
        curr.merge();

        // fix dependency errors from merge
        for(PhraseChunkNode m : merged){
          // to indicate that the node was merged, set it's index to -1
          m.index = -1;
          if(node2dependsOn.containsKey(m)){
            for(PhraseChunkNode dependedOnM : node2dependsOn.get(m)){
              // makeBloomFilter the node that depended on m now depend on the node that
              // m merged into 
              dependedOnM.depends = curr;
            }
          }
        }
      }
    }

    // re-index the merged list
    int newIndex = 1;
    for(int ii = 0; ii < nodes.size(); ii++){
      curr = nodes.get(ii);
      if(curr.index == -1){
        nodes.remove(ii);
        ii--;
      } else {
        curr.index = newIndex;
        newIndex++;
      }
    }

    // fix depends head index to match merged state
    // and makeBloomFilter final chunked hazy token list
    List<TextPosNerDpOffset> chunked = new ArrayList<TextPosNerDpOffset>();
    int nMergedTokens = nodes.size();
    for(int ii = 0; ii < nMergedTokens; ii++){
      curr = nodes.get(ii);
      if(curr.tok.dependencyIndex() > 0){
        curr.tok.setDependencyIndex(curr.depends.index); 
      }
      chunked.add(curr.tok);
    }

    return chunked;
  }

}