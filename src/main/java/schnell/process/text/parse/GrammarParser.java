package schnell.process.text.parse;

import java.util.List;

import schnell.struct.text.TextPosNer;
import schnell.struct.text.TextPosNerDp;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface GrammarParser {

  /**
   * 
   * @param tagged
   * @return
   * @throws Exception
   */
  public List<TextPosNerDp> parse(List<TextPosNer> tagged) throws Exception;
  
}