package schnell.process.text.parse;

import org.maltparser.MaltParserService;
import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import schnell.data.fmt.CONLLFmt;
import schnell.process.ConverterExcept;
import schnell.struct.StringArrayFormatter;
import schnell.struct.text.TextPosNer;
import schnell.struct.text.TextPosNerDp;
import schnell.struct.text.parse.Dependency2HazyConverter;
import schnell.util.except.Exceptions;

import java.io.File;
import java.util.List;

/**
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class MaltParser
	implements GrammarParser {
	/* methods */

	static public String
		DEFAULT_CONFIG_PREFIX_LOC = "models/maltparser/engmalt.linear-1.7",
		DEFAULT_LOG_NAME = "maltparser.Log";
	/* constructors */
	protected StringArrayFormatter<List<TextPosNer>> fmter;
	protected ConverterExcept<DependencyStructure, List<TextPosNerDp>, Exception> cvtr;
	protected MaltParserService mpService;

	public MaltParser(
		StringArrayFormatter<List<TextPosNer>> fmter,
		ConverterExcept<DependencyStructure, List<TextPosNerDp>, Exception> cvtr,
		MaltParserService mpService) {
		setFormatter(fmter);
		setConverter(cvtr);
		setMPService(mpService);
	}

	static public MaltParser make(String configFilePathPrefix, String maltparserLogName)
		throws IllegalArgumentException, MaltChainedException {
		Exceptions.checkNonNull(configFilePathPrefix,
			"maltparser working directory and configuration file path prefix");
		File f = new File(configFilePathPrefix);

		MaltParser mp = new MaltParser(
			new CONLLFmt(),
			new Dependency2HazyConverter(),
			new MaltParserService()
		);
		mp.mpService.initializeParserModel(commandLine(f.getParent(), f.getName(), maltparserLogName));
		return mp;
	}

  /* fields */

	static public MaltParser make()
		throws IllegalArgumentException, MaltChainedException {
		return make(DEFAULT_CONFIG_PREFIX_LOC, DEFAULT_LOG_NAME);
	}

	static public String commandLine(String maltparserWorkingDir,
	                                 String configFilePathPrefix, String maltparserLogName) {
		return String.format("-w %s -c %s -lfi %s -m parse",
			maltparserWorkingDir,
			configFilePathPrefix,
			maltparserLogName);
	}

	@Override
	public List<TextPosNerDp> parse(List<TextPosNer> tagged)
		throws Exception {
		return cvtr.convert(mpService.parse(fmter.format(tagged)));
	}

	public StringArrayFormatter<List<TextPosNer>> formatter() {
		return fmter;
	}

	public void setFormatter(StringArrayFormatter<List<TextPosNer>> fmter) {
		Exceptions.checkNonNull(fmter, "formatter");
		this.fmter = fmter;
	}

	public ConverterExcept<DependencyStructure, List<TextPosNerDp>, Exception> converter() {
		return cvtr;
	}

	public void setConverter(ConverterExcept<DependencyStructure, List<TextPosNerDp>, Exception> cvtr) {
		Exceptions.checkNonNull(cvtr, "converter");
		this.cvtr = cvtr;
	}

	public MaltParserService mpService() {
		return mpService;
	}

	public void setMPService(MaltParserService mpService) {
		Exceptions.checkNonNull(mpService, "maltparser service");
		this.mpService = mpService;
	}

}