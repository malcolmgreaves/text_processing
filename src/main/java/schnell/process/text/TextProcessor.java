package schnell.process.text;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.maltparser.core.exception.MaltChainedException;
import schnell.process.text.chunk.ChunkingStrategy;
import schnell.process.text.chunk.PhraseChunker;
import schnell.process.text.parse.GrammarParser;
import schnell.process.text.parse.MaltParser;
import schnell.process.text.tag.NERTagger;
import schnell.process.text.tag.POSTagger;
import schnell.process.text.tag.StanfordNERTagger;
import schnell.process.text.tag.StanfordPOSTagger;
import schnell.run.ReadConfigFile;
import schnell.struct.text.TextPosNer;
import schnell.struct.text.TextPosNerDp;
import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.TextPosNerOffset;
import schnell.struct.text.tag.NERTagged;
import schnell.struct.text.tag.POSTagged;
import schnell.struct.text.tok.TokenPosNerDpOffset;
import schnell.struct.text.tok.TokenPosNerOffset;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class TextProcessor {

    //part of speech tagger (e.g. Stanford POS tagger)
    protected POSTagger posTagger;
    protected NERTagger nerTagger;
    // grammar parser (e.g. maltparser)
    protected GrammarParser depParser;
    //chunker
    protected PhraseChunker chunker;


    public TextProcessor(POSTagger posTagger, NERTagger nerTagger,
                         GrammarParser parser, PhraseChunker chunker) {
        setPOSTagger(posTagger);
        setNERTagger(nerTagger);
        setDepParser(parser);
        setChunker(chunker);
    }

    public TextProcessor(PhraseChunker chunker)
            throws IllegalArgumentException, IOException, ClassNotFoundException, MaltChainedException {
        this(
                StanfordPOSTagger.make(),
                StanfordNERTagger.make(),
                MaltParser.make(),
                chunker);
    }

    static public TextProcessor make(Configuration conf, Logger Log)
            throws IOException, MaltChainedException, ClassNotFoundException {

        File nerModelFi = new File(conf.get(ReadConfigFile.NER_MODEL));
        Exceptions.checkFileExists(nerModelFi, "NER model location");

        File taggingModel = new File(conf.get(ReadConfigFile.POS_TAGGER_MODEL));
        Exceptions.checkFileExists(taggingModel, "stanford POS tagger model file");

        String configFilePrefix = conf.get(ReadConfigFile.MALT_PARSER_CONFIG);

        ChunkingStrategy chunking = conf.getEnum(ReadConfigFile.CHUNK, ChunkingStrategy.IDENTITY);

        Log.info(Strings.build("making text processor with these inputs:\n",
                               "NER model loc:                        ", nerModelFi.getCanonicalPath(), "\n",
                               "POS tagger model loc:                 ", taggingModel.getCanonicalPath(), "\n",
                               "maltparser configuration file:        ", configFilePrefix, "\n",
                               "chunking strategy:                    ", chunking, "\n"
                              ));


        Log.info("loading dependency parser");
        GrammarParser grammarParser = MaltParser.make(configFilePrefix, "maltparser.Log");

        Log.info("loading POS tagger");
        POSTagger posTagger = StanfordPOSTagger.make(taggingModel);

        Log.info("loading NER tagger");
        NERTagger nerTagger = StanfordNERTagger.make(nerModelFi);

        Log.info("initializing phrase chunker");
        PhraseChunker chunker = chunking.make();

        return new TextProcessor(posTagger, nerTagger, grammarParser, chunker);
    }

    static public List<TextPosNer> convert(List<TextPosNerOffset> l) {
        List<TextPosNer> c = new ArrayList<TextPosNer>(l.size());
        for (TextPosNerOffset tok : l) {
            c.add((TextPosNer) tok);
        }
        return c;
    }

    /**
     * Performs POS and NER tagging, dependecy parsing, and phrase chunking on
     * the input text.
     *
     * @param text
     * @return
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public List<TextPosNerDpOffset> process(String text)
            throws IllegalArgumentException, Exception {
        Exceptions.checkNonNull(text, "input text to process");

        // tag and parse the input text
        List<TextPosNerDpOffset> parsedAndTagged = parse(text);

        // perform nounphrase chunking on the parsed and tagged text
        List<TextPosNerDpOffset> chunked = chunker.chunk(parsedAndTagged);

        return chunked;
    }

    /**
     * Performs POS and NER tagging and dependency parsing on the input text.
     *
     * @param text
     * @return
     * @throws Exception
     */
    public List<TextPosNerDpOffset> parse(String text)
            throws Exception {
        // perform POS and NER tagging on the input text
        List<TextPosNerOffset> tagged = tag(text);

        // run the grammar parser on the tagged text
        List<TextPosNerDpOffset> record = new ArrayList<TextPosNerDpOffset>(tagged.size());

        int ii = -1;
        TextPosNerOffset t;
        for (TextPosNerDp tok : depParser.parse(convert(tagged))) {
            ii++;
            t = tagged.get(ii);
            record.add(new TokenPosNerDpOffset(tok, t.beg(), t.end()));
        }

        return record;
    }

    /**
     * Performs POS and NER tagging on the input text.
     *
     * @param text
     * @return
     */
    public List<TextPosNerOffset> tag(String text) {
        Exceptions.checkNonNull(text, "text to POS and NER tag");

        // tag the document's text with part of speech tags
        List<POSTagged> posTagged = posTagger.tag(text);

        // tag the document's text with NER tags
        List<NERTagged> nerTagged = nerTagger.nerTag(text);

        Exceptions.checkEquals("pos tagged size & ner tagged size", posTagged.size(), nerTagged.size());
        int size = posTagged.size();
        List<TextPosNerOffset> tagged = new ArrayList<TextPosNerOffset>(size);
        POSTagged pos;
        NERTagged ner;

        int offset = 0;
        for (int ii = 0; ii < size; ii++) {
            pos = posTagged.get(ii);
            ner = nerTagged.get(ii);
            if (!pos.text().equals(ner.text())) {
                throw new IllegalStateException("word mismatch on index " + ii + ", pos has word: " +
                                                pos.text() + " and ner has word: " + ner.text());
            }
            offset = text.indexOf(pos.text(), offset);

            tagged.add(new TokenPosNerOffset(pos.text(), pos.posTag(), ner.nerTag(),
                                             offset, offset + pos.text().length()));
            // don't forget about the space (+1)
            offset += pos.text().length() + 1;
        }

        return tagged;
    }

    public POSTagger posTagger() {
        return posTagger;
    }

    public void setPOSTagger(POSTagger newTagger) {
        Exceptions.checkNonNull(newTagger, "POS tagger");
        this.posTagger = newTagger;
    }

    public NERTagger nerTagger() {
        return nerTagger;
    }

    public void setNERTagger(NERTagger newTagger) {
        Exceptions.checkNonNull(newTagger, "NER tagger");
        this.nerTagger = newTagger;
    }

    public GrammarParser depParser() {
        return depParser;
    }

    public void setDepParser(GrammarParser parser) {
        Exceptions.checkNonNull(parser, "grammar parser");
        this.depParser = parser;
    }

    public PhraseChunker chunker() {
        return chunker;
    }

    public void setChunker(PhraseChunker chunker) {
        Exceptions.checkNonNull(chunker, "chunker");
        this.chunker = chunker;
    }

}