package schnell.run;


import org.apache.log4j.Logger;
import schnell.util.Strings;
import schnell.util.Sys;

/**
 * Runs a Runner.
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class Runner {

	/**
	 * Runs input Runner and uses input Logger for logging memory use,
	 * execution time, failures, and any other output.
	 */
	static public int run(Logger Log, Runnable r) {
		org.junit.runner.Runner s;
		int returnCode = 0;
		Log.info(Strings.build("[runner] STARTING...\n", Sys.memoryUsedStr()));

		long startTime = System.nanoTime();
		try {

			r.run();

		} catch (Exception e) {
			Log.fatal("[runner] Unrecoverable error encountered during run", e);
			returnCode = -1;
		}
		long endTime = System.nanoTime();

		Log.info(Strings.build("[runner] FINISHED, time elapsed: ",
			Sys.durationFormat(startTime, endTime), " (", (endTime - startTime),
			"  ns)\n", Sys.memoryUsedStr()));

		return returnCode;
	}

}