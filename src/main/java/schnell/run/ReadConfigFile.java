package schnell.run;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;
import schnell.data.io.IO;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class ReadConfigFile {
    static final public String
            TOTAL_SIZE_TO_WRITE = "total_size_to_write",
            NEG_TO_POS_RATIO = "neg_to_pos_ratio",
            MAX_ARG_DISTANCE = "max_arg_distance",
            ANSWERS = "answers",
            PEEK = "peek", // general

    // for RTextProcess
    BUFFER = "buffer",
            QUEUE_LIMIT = "queue_limit",
    // for Distant Labler (Local_ and MR_)
    BEFORE_SIZE = "before_size",
            AFTER_SIZE = "after_size",
            SKIP_NGRAM_SIZE = "skip_ngram_size",
            SKIP_SIZE = "skip_size",
            MAP_PEEK = "map_peek",
            REDUCE_PEEK = "reduce_peek",
            RAND_SELECT_PROB = "rand_select_prob",
            KB_CONTROLLER_TYPE = "kb_controller_type",
            FEAT_COUNT_NORM = "feat_count_norm",
            INPUT_PATH = "input_path",
            KB_PATH = "kb_path",
            RELATION_MAP = "relation_map",
            ONTOLOGY_PATH = "ontology_path",
            TARGET_RELATION_ID = "target_relation_id",
            DIRECTED_DISTANCE = "directed_distance",
            UNDIRECTED_DISTANCE = "undirected_distance",
            SIMILARITY_THRESHOLD = "similarity_threshold",
            NUMBER_OF_SIMILAR_CANDIDATES = "number_of_similar_candidates",
            EXPECTED_NUMBER_OF_ELEMENTS = "expected_number_of_elements",
            FALSE_POSITIVE_PROBABILITY = "false_positive_probability",

    KEY_PROCESSED_CHECK = "keys_processed_check",
            PATH_CONVERTER_TYPE = "path_converter_type",
            LEX_NGRAM = "lex_ngram",
            SYN_NGRAM = "syn_ngram",
            CHUNK = "chunk",
            SYNC_MAIN_VERB = "syntactic_main_verb",

    MAKE_SF_TYPE = "make_sf_type",

    VERBOSITY_INT = "verbosity_int",
            VERBOSITY_LEVEL = "verbosity_level",

    PRESERVE_SENTENCE_ID = "preserve_sentence_id",

    OUTPUT_TYPE = "output_type",
            VOWPAL_WABBIT = "vw",
            EXTRACT_LIST = "el",
            ALL_PAIRS = "ap",

    EXTRACTOR_TYPE = "extractor_type",
            THRESHOLD_DOUBLE = "threshold_double",
            THRESHOLD_LONG = "threshold_long",

    NONE = "none",

    N_REDUCERS = "num_reducers",

    GENERALIZE_ARG1_ARG2_FEATURES = "generalize_arg1_arg2_features",
            SWAP_ARGUMENTS = "swap_arguments",

    FEATURE_EXTRACTORS = "feature_extractors",

    USE_INTEGER_VALUE = "use_integer_value";
    // Slot filling (test) pipeline options
    static final public String
            LOG_LEVEL = "log_level",
            LOG_OUTPUT_PATH = "log_output_path",
            DO_STDERR_LOG = "do_stderr_log",

    FIRST_CLS_DIR = "first_level_cls_dir",
            SECOND_CLS_DIR = "second_level_cls_dir",

    NAME_DIC_DIR = "name_dic_dir",
            NEWSWIRE_INDEX = "newswire_index",
            WEB_INDEX = "web_index",
            DISCUSSION_INDEX = "discussion_index",
            MAX_DOCS = "max_docs",
            MAX_TOK_SAFE = "max_tok_safe",
            QUERY_MATCHER_TYPE = "query_matcher_type",
            QUERY_SCORE_THRESH = "query_score_threshold",
            QUERY_TYPE = "query_type",

    POS_TAGGER_MODEL = "pos_tagger_model",
            MALT_PARSER_CONFIG = "malt_parser_config",
            NER_MODEL = "ner_model",

    N_THREADS = "n_threads",
            LOCATION_SOURCES = "location_sources",
            CONCEPTS = "concepts",
            NOUNPHRASE_CONCEPTS = "nounphrase_concepts",
            CONCEPTS_APPENDER_SERFILE = "concepts_appender",

    PER_SCORE_THRESH = "per_score_threshold",
            ORG_SCORE_THRESH = "org_score_threshold",
            DISTANCE_METRIC = "distance_metric",
            EXACT_BOOL = "exact",

    NOUN_ONLY_ARG = "noun_only_arg",

    REL_TO_ACCEPTABLE_NER = "rel_to_acceptable_ner",
            TOP_K = "top_k",
            PREDICTION_LEVEL = "prediction_level",
            CLASSIFIER = "classifier",

    INST_CREATE_SRC = "inst_create_src",

    // multi-sentence corpus walking
    EPSILON = "epsilon",
            RESET_PROB = "reset_prob",
            TOTAL_PHRASE_COUNT = "total_phrase_count",
            PHRASE_COUNT_DIR = "phrase_count_dir";
    static protected Logger log = Logger.getLogger(ReadConfigFile.class);

    static public Configuration produce(File configFile)
            throws IllegalArgumentException, IOException {
        Exceptions.checkFileExists(configFile, "configuration file");
        BufferedReader br = IO.reader(configFile);
        Configuration conf = produce(br);
        br.close();
        return conf;
    }

    static public Configuration produce(BufferedReader br)
            throws IllegalArgumentException, IOException {
        Configuration conf = new Configuration();
        String[] bits;

        log.debug("Loading configuration settings...");
        for (String line; (line = br.readLine()) != null; ) {
            // comments start with "#"
            if (!line.substring(0, 1).equals("#")) {
                bits = line.trim().split("=");
                // configuration option is the string to the left of the "="

                if (bits[0].equals("addResource")) {
                    conf.addResource(new Path(bits[1]));
                    System.err.println("addResource: " + new Path(bits[1]));

                } else {
                    // the value is the string to the right of "=", without newline
                    conf.set(bits[0], bits[1]);
                    log.debug(Strings.build("\t", line));
                }
            }
        }

        return conf;
    }

}
