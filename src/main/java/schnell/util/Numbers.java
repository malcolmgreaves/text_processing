package schnell.util;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Numbers is a utility class that aggregates functions and objects that are
 * used to manipulate integer and floating-point numbers.
 *
 * @author Malcolm Greaves
 */
public class Numbers {

	static public final double
		EPSILON_XS = 1e-100,
		EPSILON_DOUBLE_EQUALS = 1e-25,
		EPSILON = 1e-5;
	static public final Random
		QRAND = new Random(),        // faster; lower-quality random numbers
		SRAND = new SecureRandom(),  // slower; higher-quality random numbers
		RAND = new Random();

	static public boolean isProbability(double p) {
		return (p >= 0.0) && (p <= 1.0);
	}

	/**
	 * Performs a near-exact value match.
	 *
	 * @param epsilon equality threshold
	 * @param a number to compare
	 * @param b number to compare
	 *
	 * @return Math.abs(a-b) <= epsilon
	 */
	static public boolean equals(double epsilon, double a, double b) {
		return Math.abs(a - b) <= epsilon;
	}

	/**
	 * Performs an exact value match.
	 *
	 * @param a number to compare
	 * @param b number to compare
	 *
	 * @return Double.doubleToLongBits(a) == Double.doubleToLongBits(b)
	 */
	static public boolean equals(double a, double b) {
		return Double.doubleToLongBits(a) == Double.doubleToLongBits(b);
	}

	static public double euclidianDistance(double[] v1, double[] v2) {
		if (v1.length != v2.length) {
			throw new IllegalArgumentException("v1 and v2 have different sizes!! "
				+ v1.length + " vs " + v2.length);
		}
		int size = v1.length;
		double dist = 0.0, term;
		for (int ii = 0; ii < size; ii++) {
			term = (v1[ii] - v2[ii]);
			dist += term * term; // += (v1_i - v2_i)^2
		}
		dist = Math.sqrt(dist); // sqrt( (v1_1 - v2_1)^2 + ... + (v1_n - v2_n)^2) )
		return dist;
	}

	/**
	 * Normalizes the array in-place. Returns a reference to the input, now
	 * normalized, array.
	 */
	static public double[] normalize(double[] array) {
		double sum = sum(array);
		int size = array.length;
		for (int ii = 0; ii < size; ii++) {
			array[ii] /= sum;
		}
		return array;
	}

	static public double sum(double[] array) {
		double sum = 0.0;
		int size = array.length;
		for (int ii = 0; ii < size; ii++) {
			sum += array[ii];
		}
		return sum;
	}

}