package schnell.util.func;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public interface Func {

	/**
	 * Identity function.
	 */
	static public final Real IDENTITY = new Real() {
		@Override
		public double evaluate(double x) {
			return x;
		}
	};
	/**
	 * Absolute value function.
	 */
	static public final Real ABSVAL = new Real() {
		@Override
		public double evaluate(double x) {
			return Math.abs(x);
		}
	};
	/**
	 * Square function.
	 */
	static public final Real SQUARE = new Real() {
		@Override
		public double evaluate(double x) {
			return x * x;
		}
	};
	/**
	 * Square root function.
	 */
	static public final Real SQRT = new Real() {
		@Override
		public double evaluate(double x) {
			return Math.sqrt(x);
		}
	};
	/**
	 * Sigmoid function, with over and underflow clipping.
	 */
	static public final Real SIGMOID = new Real() {
		static public final double OVERFLOW = 20
			,
			NEG_OVERFLOW = -OVERFLOW;

		@Override
		public double evaluate(double score) {
			if (score > OVERFLOW) {
				score = OVERFLOW;
			} else if (score < NEG_OVERFLOW) {
				score = -OVERFLOW;
			}

			double exp = Math.exp(score);
			return exp / (1 + exp);
		}
	};

	/**
	 * UNARY FUNCTIONS
	 * y  <--  f()
	 */
	static public interface Unary<O> extends Func {
		public O evaluate();
	}

	static public interface UnaryPlus<O> extends Func {
		public O evaluate(double parameter);
	}

	static public interface UnaryReal extends Func {
		public double evaluate();
	}

	static public interface UnaryBool extends Func {
		public boolean evaluate();
	}

	/**
	 * BINARY FUNCTIONS
	 * y  <--  f(x T)
	 */
	static public interface Binary<I, O> extends Func {
		public O evaluate(I input);

		static public class Identity<X> implements Binary<X, X> {
			@Override
			final public X evaluate(X input) {
				return input;
			}
		}
	}

	static public interface BinaryPlus<I, O> extends Func {
		public O evaluate(double parameter, I input);
	}

	static public interface BinaryDoublePlus<I, O> extends Func {
		public O evaluate(double parameter1, double parameter2, I input);
	}

	static public interface RealBinary<I> extends Func {
		public double evaluate(I input);
	}

	static public interface BoolBinary<I> extends Func {
		public boolean evaluate(I input);
	}


	/**
	 * DUAL INPUT FUNCTIONS
	 * y <-- f(x T, y U)
	 */

	static public interface Dual<IN1, IN2, O> extends Func {
		public O evaluate(IN1 input1, IN2 input2);
	}

	static public interface DualPlus<IN1, IN2, O> extends Func {
		public O evaluate(double parameter, IN1 input1, IN2 input2);
	}

	static public interface DualDoublePlus<IN1, IN2, O> extends Func {
		public O evaluate(double parameter1, double parameter2, IN1 input1, IN2 input2);
	}

	static public interface DualReal<IN1, IN2, O> extends Func {
		public double evaluate(IN1 input1, IN2 input2);
	}

	static public interface DualBinary<IN1, IN2, O> extends Func {
		public boolean evaluate(IN1 input1, IN2 input2);
	}

	/**
	 * VERRIADIC FUNCTIONS
	 * y  <--  f(x...T)
	 */
	static public interface Verriadic<I, O> extends Func {
		public O evaluate(I... inputs);
	}

	/**
	 * REAL FUNCTIONS
	 * y double  <--  f(x double) OR f(x...double)
	 */
	static public interface VerriadicPlus<I, O> extends Func {
		public O evaluate(double parameter, I... inputs);
	}

	static public interface VerriadicReal<I> extends Func {
		public double evaluate(I... inputs);
	}

	/**
	 * FREE FUNCTIONS
	 * y  <--  f(x...Object)
	 */
	static public interface Free<O> extends Func {
		public O evaluate(Object... objects);
	}

	static public interface FreePlus<O> extends Func {
		public O evaluate(double parameter, Object... objects);
	}

	static public interface FreeReal extends Func {
		public double evaluate(Object... objects);
	}

	/**
	 * Real function.
	 */
	static public interface Real extends Func {
		public double evaluate(double x);
	}

	/**
	 * Verriadic function.
	 */
	static public interface RealVerriadic extends Func {
		public double evaluate(double... x);
	}

	/**
	 * SIDE EFFECT FUNCTIONS
	 * ()  <--  f()
	 * HAS MUTATION
	 * CAN THROW EXCEPTIONS
	 */
	static public interface SideEffect extends Func {
		public void evaluate() throws Exception;
	}

	/**
	 * SIDE EFFECT FUNCTIONS
	 * ()  <--  f(input I)
	 * HAS MUTATION
	 * CAN THROW EXCEPTIONS
	 */
	static public interface Void<I> extends Func {
		public void evaluate(I input) throws Exception;
	}

	/**
	 * SIDE EFFECT FUNCTIONS
	 * ()  <--  f(input I)
	 */
	static public interface VoidClean<I> extends Func {
		public void evaluate(I input);
	}

}