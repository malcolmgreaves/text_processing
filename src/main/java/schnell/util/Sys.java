package schnell.util;

import java.util.concurrent.TimeUnit;


/**
 * Various system or runtime related functions.
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class Sys {

	/**
	 * Returns the duration between start and end, assuming that start < end and that
	 * both numbers are nanoseconds. The returned format is %d hours %d minutes %s seconds .
	 */
	static public String durationFormat(long startNanoTime, long endNanoTime) {
		final long
			millis = endNanoTime - startNanoTime,
			hours = TimeUnit.NANOSECONDS.toHours(millis),
			minutes = TimeUnit.NANOSECONDS.toMinutes(millis) - hours * 60,
			seconds = TimeUnit.NANOSECONDS.toSeconds(millis) - minutes * 60 - hours * 3600;
		return String.format("%d hours, %d min, %d sec", hours, minutes, seconds);
	}

	/**
	 * Returns the difference in the runtime's total memory and free memory,
	 * resulting in the number of bytes of used memory.
	 */
	static public long memoryUsed() {
		return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	}

	/**
	 * Formats information about current memory useage, free memory, allocated memory,
	 * and maximum memory the JVM can use. Returns this information as a human-
	 * readable string
	 */
	static public String memoryUsedStr() {
		final long
			maxMem = Runtime.getRuntime().maxMemory(),
			totalMem = Runtime.getRuntime().totalMemory(),
			freeMem = Runtime.getRuntime().freeMemory(),
			usedMem = Sys.memoryUsed();

		return String.format("::MEMORY INFORMATION::\n" +
			"Max JVM memory:         %s bytes\n" +
			"Total allocated memory: %s bytes\n" +
			"Free memory:            %s bytes\n" +
			"Used memory:            %s bytes",
			Strings.NUM_FMT.format(maxMem),
			Strings.NUM_FMT.format(totalMem),
			Strings.NUM_FMT.format(freeMem),
			Strings.NUM_FMT.format(usedMem));
	}

}