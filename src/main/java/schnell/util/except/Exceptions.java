package schnell.util.except;

import schnell.util.Numbers;
import schnell.util.Strings;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Collection;


public class Exceptions {


	/**
	 * @param e an exception
	 * @return Pretty-print representation of the exception.
	 */
	static public String toString(Exception e) {
		StringBuilder sb = new StringBuilder();

		// get the cause and exception message
		sb.append("TYPE:   ").append(e.getClass()).append("\n");
		sb.append("EXCEPTION:\n").append(e.getMessage()).append("\n");
		sb.append("CAUSE:\n").append(e.getCause()).append("\n");

		// get the stack trace
		sb.append("STACK TRACE:\n");
		StackTraceElement[] stes = e.getStackTrace();
		for (StackTraceElement ste : stes) {
			sb.append("\t").append(ste.toString()).append("\n");
		}

		return sb.toString();
	}

	//
	// valid input checking functions
	//

	static public void checkIsTrue(boolean condition)
		throws IllegalArgumentException {
		checkIsTrue(condition, "input");
	}

	static public void checkIsTrue(boolean condition, String message)
		throws IllegalArgumentException {
		if (!condition) {
			throw new IllegalArgumentException(message + " is false");
		}
	}

	// for int and long s

	static public void checkIsNotZero(long value)
		throws IllegalArgumentException {
		checkIsNotZero(value, "input");
	}

	static public void checkIsNotZero(long value, String message)
		throws IllegalArgumentException {
		if (value == 0L) {
			throw new IllegalArgumentException(Strings.build(message,
				" should not be null"));
		}
	}

	static public void checkEquals(long expected, long... actuals)
		throws IllegalArgumentException {
		checkEquals("input", expected, actuals);
	}

	static public void checkEquals(String message, long expected, long... actuals)
		throws IllegalArgumentException {
		for (int ii = 0; ii < actuals.length; ii++) {
			if (actuals[ii] != expected) {
				throw new IllegalArgumentException(String.format(
					"actuals[%d]: (%d) doesn't equal expected (%d): %s",
					ii, actuals[ii], expected, message));
			}
		}
	}

	static public void checkNonNegative(long input)
		throws IllegalArgumentException {
		checkNonNegative(input, "input");
	}

	static public void checkNonNegative(long input, String nameOfInput)
		throws IllegalArgumentException {
		if (input < 0) {
			throw new IllegalArgumentException(String.format(
				"%s must be nonnegative, not %d", nameOfInput, input));
		}
	}

	static public void checkIsPositive(long input)
		throws IllegalArgumentException {
		checkIsPositive(input, "input");
	}

	static public void checkIsPositive(long input, String nameOfInput)
		throws IllegalArgumentException {
		if (input < 1) {
			throw new IllegalArgumentException(String.format(
				"%s must be positive, not %d", nameOfInput, input));
		}
	}

	static public void checkIsNegative(long input)
		throws IllegalArgumentException {
		checkIsNegative(input, "input");
	}

	static public void checkIsNegative(long input, String nameOfInput)
		throws IllegalArgumentException {
		if (input > -1) {
			throw new IllegalArgumentException(String.format(
				"%s must be negative, not %d", nameOfInput, input));
		}
	}

	static public void checkIsNonPositive(long input)
		throws IllegalArgumentException {
		checkIsNonPositive(input, "input");
	}

	static public void checkIsNonPositive(long input, String nameOfInput)
		throws IllegalArgumentException {
		if (input > 0) {
			throw new IllegalArgumentException(String.format(
				"%s must be nonpositive, not %d", nameOfInput, input));
		}
	}

	static public void checkIsNonNegative(long input)
		throws IllegalArgumentException {
		checkIsNonNegative(input, "input");
	}

	static public void checkIsNonNegative(long input, String nameOfInput)
		throws IllegalArgumentException {
		if (input < 0) {
			throw new IllegalArgumentException(String.format(
				"%s must be nonnegative, not %d", nameOfInput, input));
		}
	}

	// for double s

	/**
	 * If |value - 0| < Numbers.EPSILON_DOUBLE_EQUALS then throw IllegalArgumentException
	 *
	 * @param value
	 * @throws IllegalArgumentException
	 */
	static public void checkIsNotZero(double value)
		throws IllegalArgumentException {
		checkIsNotZero(value, "input");
	}

	/**
	 * If |value - 0| < Numbers.EPSILON_DOUBLE_EQUALS then throw IllegalArgumentException
	 *
	 * @param value
	 * @param message
	 * @throws IllegalArgumentException
	 */
	static public void checkIsNotZero(double value, String message)
		throws IllegalArgumentException {
		if (value == 0.0 || Math.abs(value - 0) < Numbers.EPSILON_DOUBLE_EQUALS) {
			throw new IllegalArgumentException(Strings.build(message,
				" should not be null"));
		}
	}

	static public void checkIsProbability(double input)
		throws IllegalArgumentException {
		checkIsProbability(input, "input");
	}

	static public void checkIsProbability(double input, String nameOfInput)
		throws IllegalArgumentException {
		if (input < 0 || input > 1.0) {
			throw new IllegalArgumentException(Strings.build(nameOfInput,
				"must be within [0,1], not ", input));
		}
	}

	// for STRING s

	static public void checkNonNullNonEmpty(String input)
		throws IllegalArgumentException {
		checkNonNullNonEmpty(input, "input");
	}

	static public void checkNonNullNonEmpty(String input, String nameOfInput)
		throws IllegalArgumentException {
		if (input == null || input.length() == 0) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null or zero length", nameOfInput));
		}
	}

	// for ANY TYPE objects

	static public <T> void checkNonNull(T input)
		throws IllegalArgumentException {
		Exceptions.checkNonNull(input, "input");
	}

	static public <T> void checkNonNull(T input, String nameOfInput)
		throws IllegalArgumentException {
		if (input == null) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null", nameOfInput));
		}
	}

	// for COLLECTION s

	static public <T> void checkNonNullNonEmpty(Collection<T> input)
		throws IllegalArgumentException {
		checkNonNullNonEmpty(input, "input");
	}

	static public <T> void checkNonNullNonEmpty(Collection<T> input, String nameOfInput)
		throws IllegalArgumentException {
		if (input == null || input.size() == 0) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null or zero length", nameOfInput));
		}
	}

	// for ARRAY s
	static public <T> void checkNonNullNonEmpty(T[] input)
		throws IllegalArgumentException {
		checkNonNullNonEmpty(input, "input");
	}

	static public <T> void checkNonNullNonEmpty(T[] input, String nameOfInput)
		throws IllegalArgumentException {
		if (input == null || input.length == 0) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null or zero length", nameOfInput));
		}
	}

	// for READERs

	static public void checkBufferReady(BufferedReader r)
		throws IllegalArgumentException, IOException {
		Exceptions.checkBufferReady(r, "reader");
	}

	static public void checkBufferReady(BufferedReader r, String msg)
		throws IllegalArgumentException, IOException {
		Exceptions.checkNonNull(r, msg);
		if (!r.ready()) {
			throw new IllegalArgumentException(Strings.build("unready reader: ", msg));
		}
	}

	// for FILE s

	static public void checkFileExists(File input)
		throws IllegalArgumentException, IOException {
		Exceptions.checkFileExists(input, "file");
	}

	static public void checkFileExists(File input, String nameOfInput)
		throws IllegalArgumentException, IOException {
		if (input == null) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null", nameOfInput));
		}
		if (!input.isFile()) {
			throw new IllegalArgumentException(String.format(
				"%s is not a file that exists, location: %s", nameOfInput, input.getCanonicalPath()));
		}
	}

	static public void checkExists(File input) {
		checkExists(input, "input");
	}

	static public void checkExists(File input, String nameOfInput) {
		Exceptions.checkNonNull(input, nameOfInput);
		if (!input.exists()) {
			throw new IllegalArgumentException(Strings.build(input, " doesn't exist on disk"));
		}
	}

	static public void checkFileDoesntExist(File input)
		throws IllegalArgumentException, IOException {
		Exceptions.checkFileDoesntExist(input, "file");
	}

	static public <T> void checkFileDoesntExist(File input, String nameOfInput)
		throws IllegalArgumentException, IOException {
		if (input == null) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null", nameOfInput));
		}
		if (input.isFile()) {
			throw new IllegalArgumentException(String.format(
				"%s is a file that exists, location: %s", nameOfInput, input.getCanonicalPath()));
		}
	}

	// for DIRECTORY s

	static public <T> void checkDirExists(File input)
		throws IllegalArgumentException, IOException {
		Exceptions.checkDirExists(input, "directory");
	}

	static public <T> void checkDirExists(File input, String nameOfInput)
		throws IllegalArgumentException, IOException {
		if (input == null) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null", nameOfInput));
		}
		if (!input.isDirectory()) {
			throw new IllegalArgumentException(String.format(
				"%s is not a directory that exists, location: %s", nameOfInput, input.getCanonicalPath()));
		}
	}

	static public <T> void checkDirDoesntExist(File input)
		throws IllegalArgumentException, IOException {
		Exceptions.checkDirDoesntExist(input, "directory");
	}

	static public <T> void checkDirDoesntExist(File input, String nameOfInput)
		throws IllegalArgumentException, IOException {
		if (input == null) {
			throw new IllegalArgumentException(String.format(
				"%s cannot be null", nameOfInput));
		}
		if (input.isDirectory()) {
			throw new IllegalArgumentException(String.format(
				"%s is a directory that exists, location: %s", nameOfInput, input.getCanonicalPath()));
		}
	}


	// for WORKING DIRECTORY

	static public <T> File initWorkingDir(File input)
		throws IllegalArgumentException, IOException {
		Exceptions.checkFileDoesntExist(input, "working directory");
		if (!input.isDirectory() && !input.mkdirs()) {
			throw new IllegalArgumentException(String.format(
				"cannot initialize primary working directory %s", input.getCanonicalPath()));
		}
		input.delete();

		File workingDir = File.createTempFile(input.getName(), "", input.getParentFile());
		workingDir.delete();
		workingDir.mkdirs();
		if (!workingDir.isDirectory()) {
			throw new IllegalArgumentException(String.format(
				"cannot initialize finalized working directory %s", workingDir.getCanonicalPath()));
		}
		return workingDir;
	}
}