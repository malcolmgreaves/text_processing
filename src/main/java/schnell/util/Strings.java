package schnell.util;

import schnell.util.except.Exceptions;
import schnell.util.func.Func;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * String manipulation and formatting functions.
 *
 * @author Malcolm Greaves
 */
public class Strings {

	//
	// Uncategorized, general string functions
	//

	static public final String NEWLINE = System.getProperty("line.separator");
	static public final String
		/**
		 * string representation of any null object
		 */
		NULL = "(null)";
	static public final DecimalFormat
		/**
		 * Standard number format pattern: "###,###.###"
		 * For example, format "123456.78" as "123,456.78"
		 */
		NUM_FMT = new DecimalFormat("###,###.###"),
	/**
	 * Scientific notation format pattern: "0.#####E0"
	 * or ####.####e#### ??
	 */
	SCI_NOT = new DecimalFormat("0.#####E0"),
	/**
	 * Probability notation format pattern: "0.######"
	 */
	PROB = new DecimalFormat("0.######");

	/**
	 * Append a newline character "\n" to the input string iff there isn't one.
	 *
	 * @param l input
	 * @return l such that l has a newline as its last character
	 */
	static public String idempotentAppendNewline(String l) {
		if (!l.endsWith("\n")) {
			return String.format("%s\n", l);
		}
		return l;
	}

	static public String build(Object... parts) {
		if (parts == null || parts.length == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (Object part : parts) {
			sb.append(part == null ? "null" : part.toString());
		}
		return sb.toString();
	}

	static public <T> String build(Collection<T> parts) {
		if (parts == null || parts.size() == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (T part : parts) {
			if (part != null) {
				sb.append(part.toString());
			}
		}
		return sb.toString();
	}

	static public <T> String build(Iterator<T> parts) {
		if (parts == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		T part;
		while (parts.hasNext()) {
			part = parts.next();
			if (part != null) {
				sb.append(part.toString());
			}
		}
		return sb.toString();
	}
	//
	// SPLIT functions
	//

	/**
	 * Safely returns the string representation of its input. If the input is
	 * null, then safe returns the NULL string (which is "(null)").
	 *
	 * @param x input
	 * @return string representation, or "(null)" if the input is null
	 */
	static public <T> String safe(T x) {
		return (x == null) ? NULL : x.toString();
	}

	/**
	 * Splits the input line by tabs ("\t"), returns resulting string array.
	 *
	 * @param line input
	 * @return split result
	 */
	static public String[] tabSplit(String line) {
		return line.trim().split("\\t");
	}

	/**
	 * Splits the input line by commas (",") and returns the resulting string array.
	 *
	 * @param line input
	 * @return split result
	 */
	static public String[] commaSplit(String line) {
		return line.trim().split(",");
	}

	//
	// JOIN functions
	//

	/**
	 * Splits the input line by spaces (" "), returns resulting string array.
	 *
	 * @param line input
	 * @return split result
	 */
	static public String[] spaceSplit(String line) {
		return line.trim().split(" ");
	}

	/**
	 * @param joiner String to join the parts together with.
	 * @param parts  The array of elements to join together.
	 * @return Joined string of all parts.
	 */

	static public <T> String join(String joiner, T... parts)
		throws IllegalArgumentException {
		if (joiner == null || parts == null || parts.length == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		Object part;
		for (int ii = 0; ii < parts.length; ii++) {
			part = parts[ii];
			if (part != null) {
				sb.append(part.toString()).append(joiner);
			}
		}
		Strings.rmLastSubstr(sb, joiner);
		return sb.toString();
	}

	/**
	 * @param joiner String to join the parts together with.
	 * @param parts  Elements iterator of parts to join together.
	 * @return Joined string of all parts.
	 */
	static public <T> String join(String joiner, Collection<T> parts)
		throws IllegalArgumentException {
		if (joiner == null || parts == null || parts.size() == 0) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (T part : parts) {
			if (part != null) {
				sb.append(part.toString()).append(joiner);
			}
		}
		Strings.rmLastSubstr(sb, joiner);
		return sb.toString();
	}

	static public <T> Collection<String> map(Func.Binary<T, String> f, Collection<T> parts)
		throws IllegalArgumentException {
		Exceptions.checkNonNull(f, "mapping function");
		Exceptions.checkNonNull(parts, "parts to map");

		List<String> stringifiedParts = new ArrayList<String>(parts.size());
		for (T part : parts) {
			stringifiedParts.add(f.evaluate(part));
		}
		return stringifiedParts;
	}

	//
	// StringBuilder functions
	//

	/**
	 * @param joiner String to join the parts together with.
	 * @param parts  Elements iterator of parts to join together.
	 * @return Joined string of all parts.
	 */
	static public <T> String join(String joiner, Iterator<T> parts) {
		if (joiner == null || parts == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		T part;
		while (parts.hasNext()) {
			part = parts.next();
			if (part != null) {
				sb.append(part.toString()).append(joiner);
			}
		}
		Strings.rmLastSubstr(sb, joiner);
		return sb.toString();
	}

	/**
	 * Removes the last character from the input string builder.
	 *
	 * @param sb the string builder.
	 */
	static public void rmLastChar(StringBuilder sb) {
		if (sb.length() > 0) {
			int last = sb.length() - 1;
			sb.replace(last, last + 1, "");
		}
	}

	//
	// Formatting arrays and collections of strings to numerical types.
	//

	/**
	 * Removes the last input substring from the input string builder.
	 *
	 * @param sb         the string builder
	 * @param substrToRm suffix to remove from sb
	 */
	static public void rmLastSubstr(StringBuilder sb, String substrToRm) {
		int sbLen = sb.length(),
			lastSubstrToRmIndex = sbLen - substrToRm.length();
		if (sb.indexOf(substrToRm, lastSubstrToRmIndex) > 0) {
			sb.replace(lastSubstrToRmIndex, sbLen, "");
		}
	}

	/**
	 * Treats each stngri in the input array as a double.
	 *
	 * @param bits
	 * @return
	 */
	static public double[] convDouble(String[] bits)
		throws NumberFormatException {
		double[] values = new double[bits.length];
		for (int ii = 0; ii < bits.length; ii++) {
			values[ii] = Double.parseDouble(bits[ii]);
		}
		return values;
	}

	static public double[] convDouble(Collection<String> bits)
		throws NumberFormatException {
		double[] values = new double[bits.size()];
		int ii = 0;
		for (String bit : bits) {
			values[ii++] = Double.parseDouble(bit);
		}
		return values;
	}

	static public double[] convDouble(Iterator<String> bits)
		throws NumberFormatException {
		List<Double> list = new ArrayList<Double>();
		while (bits.hasNext()) {
			list.add(Double.parseDouble(bits.next()));
		}
		double[] values = new double[list.size()];
		for (int ii = 0; ii < list.size(); ii++) {
			values[ii] = list.get(ii);
		}
		return values;
	}

	/**
	 * Treats each string in the input array as an int.
	 *
	 * @param bits
	 * @return
	 */
	static public int[] convInt(String[] bits)
		throws NumberFormatException {
		int[] values = new int[bits.length];
		for (int ii = 0; ii < bits.length; ii++) {
			values[ii] = Integer.parseInt(bits[ii]);
		}
		return values;
	}

	static public int[] convint(Collection<String> bits)
		throws NumberFormatException {
		int[] values = new int[bits.size()];
		int ii = 0;
		for (String bit : bits) {
			values[ii++] = Integer.parseInt(bit);
		}
		return values;
	}


}