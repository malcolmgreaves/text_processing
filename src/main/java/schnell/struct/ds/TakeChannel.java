package schnell.struct.ds;

/**
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 * @param <OUT> output type
 */
public interface TakeChannel<OUT> {

  /**
   * 
   * @return
   */
  public Iterable<OUT> take();
  
}