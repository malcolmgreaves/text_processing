package schnell.struct.ds;

import schnell.util.except.Exceptions;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public class SyncBiChannel<IN, OUT> {

	protected BiChannel<IN, OUT> chan;

	public SyncBiChannel(BiChannel<IN, OUT> chan) {
		Exceptions.checkNonNull(chan, "channel");
		this.chan = chan;
	}

	/**
	 * synchronizes acess on channel
	 */
	public void put(IN input) {
		synchronized (chan) {
			chan.put(input);
		}
	}

	/**
	 * synchronizes acess on channel
	 * may return null
	 */
	public Iterable<OUT> take() {
		Iterable<OUT> values = null;
		synchronized (chan) {
			values = chan.take();
		}
		return values;
	}

}