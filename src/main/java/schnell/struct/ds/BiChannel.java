package schnell.struct.ds;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 * @param <IN> input type
 * @param <OUT> output type
 */
public interface BiChannel<IN,OUT>
extends PutChannel<IN>, TakeChannel<OUT> {

}