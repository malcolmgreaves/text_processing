package schnell.struct.ds;

import java.io.Serializable;

/**
 * Represents a pair of values. We use it a lot for pairs of feature IDs and
 * values.
 * 
 * @author acarlson -- copied from
 *         http://forum.java.sun.com/thread.jspa?threadID=5132045
 */
public class Pair<L, R>
implements Serializable {
  static final protected long serialVersionUID = 6830737850404982819L;

  public L left;
  public R right;

  /**
   * Constructor for a pair of values.
   * 
   * @param left
   *                The left value
   * @param right
   *                The right value
   */
  public Pair(L left, R right) {
    this.left = left;
    this.right = right;
  }

  /**
   * Factory method that creates an object that is a Pair of values.
   * 
   * @param left
   *                The left value
   * @param right
   *                The right value
   * @return The Pair object containing the specified values.
   */
  static public <A, B> Pair<A, B> create(A left, B right) {
    return new Pair<A, B>(left, right);
  }

  /**
   * Does element wise comparision for equality.
   * 
   * @param o Other object.
   * @return True iff this equals o. False otherwise.
   */
  public boolean equals(Object o) {
    if (o == null || !Pair.class.isInstance(o)) return false;
    Pair<?,?> other = (Pair<?,?>) o;
    return  equal(this.left, other.left) && equal(this.right, other.right);
  }

  /**
   * 
   * @param o1
   * @param o2
   * @return True iff o1.equals(o2) is true. False otherwise or if either o1,o2 is null.
   */
  static public boolean equal(Object o1, Object o2) {
    if (o1 == null){
      return o2 == null;
    }
    return o1.equals(o2);
  }

  /**
   * @return A hash code for the object.
   */
  public int hashCode() {
    int l2hash = left == null ? 0 : left.hashCode();
    int r2hash = right == null ? 0 : right.hashCode();
    return l2hash + (57 * r2hash);
  }

  public String toString() {
    String l2str = (left == null) ? "null" : left.toString();
    String r2str = (right== null) ? "null" : right.toString();
    return String.format("(%s,%s)", l2str, r2str);
  }


  /**
   * Allows one to do this:
   * 
   * TreeSet<Pair.AscendLeftComparable<Double, String>> listOfPairsToSort;
   * listOfPairsToSort = new TreeSet<Pair.AscendLeftComparable<Double, String>>();
   * ...
   * ...
   * ...add some stuff to listOfPairsToSort...
   * ...
   * ...
   * ---can now traverse things in the set in sorted order, based upon the LEFT element of each pair---
   * 
   * 
   * @author Malcolm Greaves
   * 
   * @param <L> Left
   * @param <R> Right
   */
  static public class AscendLeftComparable<L extends Comparable<? super L>, R>
  extends Pair<L,R> implements Comparable<AscendLeftComparable<L,R>> {
    static final protected long serialVersionUID = -2072767612246174220L;
    public AscendLeftComparable(L left, R right){
      super(left,right);
    }
    @Override
    public int compareTo(AscendLeftComparable<L, R> other){
      return this.left.compareTo(other.left);
    }
  }
  static public class DescendLeftComparable<L extends Comparable<? super L>, R>
  extends Pair<L,R> implements Comparable<DescendLeftComparable<L,R>> {
    static final protected long serialVersionUID = -1998920748448848409L;
    public DescendLeftComparable(L left, R right){
      super(left,right);
    }
    @Override
    public int compareTo(DescendLeftComparable<L, R> other){
      return -this.left.compareTo(other.left);
    }
  }


  /**
   * Allows one to do this:
   * 
   * TreeSet<Pair.DescendRightComparable<String,Double>> listOfPairsToSort;
   * listOfPairsToSort = new TreeSet<Pair.DescendRightComparable<String,Double>>();
   * ...
   * ...
   * ...add some stuff to listOfPairsToSort...
   * ...
   * ...
   * ---can now traverse things in the set in sorted order, based upon the RIGHT element of each pair---
   * 
   * 
   * @author Malcolm Greaves
   * 
   * @param <L> Left
   * @param <R> Right
   */
  static public class AscendRightComparable<L, R extends Comparable<? super R>>
  extends Pair<L,R> implements Comparable<AscendRightComparable<L,R>> {
    static final protected long serialVersionUID = -7999220298351203724L;
    public AscendRightComparable(L left, R right){
      super(left,right);
    }
    @Override
    public int compareTo(AscendRightComparable<L, R> other){
      return this.right.compareTo(other.right);
    }
  }
  static public class DescendRightComparable<L, R extends Comparable<? super R>>
  extends Pair<L,R> implements Comparable<DescendRightComparable<L,R>> {
    static final protected long serialVersionUID = -4805661337072500976L;
    public DescendRightComparable(L left, R right){
      super(left,right);
    }
    @Override
    public int compareTo(DescendRightComparable<L, R> other){
      return -this.right.compareTo(other.right);
    }
  }
}