package schnell.struct.ds;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class DoubleIncremental  {
  /* methods */

  public void increment(double valToAdd){
    val += valToAdd;
  }

  /* constructors */

  public DoubleIncremental(double val){
    this.val = val;
  }

  public DoubleIncremental(Number val){
    this.val = val.doubleValue();
  }

  public DoubleIncremental(DoubleIncremental val){
    this.val = val.val;
  }

  /* fields */

  //the actual value that we're wrapping
  protected double val;
  public double value(){ return val; }
  public void setValue(double val){ this.val = val; }

}