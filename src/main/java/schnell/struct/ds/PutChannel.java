package schnell.struct.ds;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 *@param IN input type
 */
public interface PutChannel<IN> {
  
  /**
   * 
   * @param input
   */
  public void put(IN input);

}
