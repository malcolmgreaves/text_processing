package schnell.struct.ds;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import schnell.run.ReadConfigFile;
import schnell.util.Strings;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public enum InstanceCreationSource {
	LUCENE, TEXT, PRE_PROCESSED, INVALID, ALL_DOC, TEXT_DOCSENTID;

	static public InstanceCreationSource make(Configuration conf, Logger log) {
		InstanceCreationSource instCreateSrc = conf.getEnum(ReadConfigFile.INST_CREATE_SRC, InstanceCreationSource.INVALID);
		if (instCreateSrc == InstanceCreationSource.INVALID) {
			throw new IllegalArgumentException(Strings.build("*MUST* have value for ",
				ReadConfigFile.INST_CREATE_SRC, " = { ", InstanceCreationSource.LUCENE, " , ",
				InstanceCreationSource.TEXT, " , ", InstanceCreationSource.PRE_PROCESSED, ", ",
				InstanceCreationSource.TEXT_DOCSENTID, " }"));
		}

		log.info(Strings.build("Instance Creation Source:             ", instCreateSrc, "\n"));
		return instCreateSrc;
	}

}