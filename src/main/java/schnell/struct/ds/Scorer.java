package schnell.struct.ds;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface Scorer {
  
  public double score();
  
  public boolean setScore(double newScore);
  
}