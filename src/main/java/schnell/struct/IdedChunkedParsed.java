package schnell.struct;

import schnell.struct.text.TextPosNerDpOffset;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.util.List;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class IdedChunkedParsed {
  protected String docsentID;
  protected List<TextPosNerDpOffset> chunkedAndParsed;

  public IdedChunkedParsed(String docsentID, List<TextPosNerDpOffset> chunkedAndParsed) {
    setDocsentID(docsentID);
    setChunkedAndParsed(chunkedAndParsed);
  }

  public String toString() {
    return Strings.build("(", docsentID, "): ", chunkedAndParsed.toString());
  }

  public String docID() {
    return docsentID.substring(0, docsentID.indexOf(".S"));
  }

  public String docsentID() {
    return docsentID;
  }

  public void setDocsentID(String docsentID) {
    Exceptions.checkNonNull(docsentID, "document-sentence ID");
    if (!docsentID.contains(".S"))
      throw new IllegalArgumentException("document-sentence ID needs to have format xxx.S#");
    this.docsentID = docsentID;
  }

  public List<TextPosNerDpOffset> chunkedAndParsed() {
    return chunkedAndParsed;
  }

  public void setChunkedAndParsed(List<TextPosNerDpOffset> chunkedAndParsed) {
    Exceptions.checkNonNull(chunkedAndParsed, "chunked & parsed");
    this.chunkedAndParsed = chunkedAndParsed;
  }

}