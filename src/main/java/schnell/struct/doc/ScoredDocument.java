package schnell.struct.doc;


import schnell.struct.ds.Scorer;
import schnell.struct.text.Text;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class ScoredDocument
    implements Scorer, DocID, Text {

  protected String docsentID, text;
  protected double score;

  /**
   * @param score
   */
  public ScoredDocument(String docsentID, double score, String text) {
    setDocID(docsentID);
    setScore(score);
    setText(text);
  }

  @Override
  public String docID() {
    return docsentID.substring(0, docsentID.lastIndexOf(".S"));
  }

  public String docsentID() {
    return docsentID;
  }

  @Override
  public void setDocID(String docsentID) {
    Exceptions.checkNonNull(docsentID, "document sentence ID");
    this.docsentID = docsentID;
  }

  public String text() {
    return text;
  }

  @Override
  public void setText(String text) {
    Exceptions.checkNonNull(text, "text");
    this.text = text;
  }

  @Override
  public String toString() {
    return Strings.build("[", score, ":", docsentID, "] ", text);
  }

  /**
   * Two DocLuceneScore objects are equal if their text() are equal().
   */
  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    } else if (o == this) {
      return true;
    } else if (!(o instanceof ScoredDocument)) {
      return false;
    }
    try {
      ScoredDocument other = (ScoredDocument) o;
      return docsentID().equals(other.docsentID()) && text().equals(other.text());

    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return docsentID.hashCode() + text.hashCode() * 31;
  }

  /**
   * @return score of doucmnet, as returned by the lucene search
   */
  @Override
  public double score() {
    return score;
  }

  @Override
  public boolean setScore(double newScore) {
    this.score = newScore;
    return true;
  }

}