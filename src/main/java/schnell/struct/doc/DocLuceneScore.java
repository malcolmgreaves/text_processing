package schnell.struct.doc;

import org.apache.lucene.document.Document;
import schnell.data.index.search.Querier;
import schnell.struct.ds.Scorer;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class DocLuceneScore
    implements Scorer, DocID {

  // document; result from lucene search
  final protected Document doc;
  final protected String docContents;
  protected String docsentID;
  // document score
  protected double score;

  /**
   * @param score
   * @param doc   must be non null, have non-null and non-empty "contents" and "docsentid"
   */
  public DocLuceneScore(double score, Document doc) {
    setScore(score);
    Exceptions.checkNonNull(doc, "document");
    this.doc = doc;
    this.docContents = doc.get(Querier.CONTENTS);
    Exceptions.checkNonNullNonEmpty(docContents, "document's contents");
    setDocID(doc.get(Querier.DOCSENTID));
  }

  @Override
  public String docID() {
    return docsentID.substring(0, docsentID.lastIndexOf(".S"));
  }

  public String docsentID() {
    return docsentID;
  }

  @Override
  public void setDocID(String docID) {
    Exceptions.checkNonNull(docID, "document sentence ID");
    this.docsentID = docID;
  }

  public String text() {
    return docContents;
  }

  @Override
  public String toString() {
    return Strings.build("[", score, ":", docsentID, "] ", text());
  }

  /**
   * Two DocLuceneScore objects are equal if their text() are equal().
   */
  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    } else if (o == this) {
      return true;
    } else if (!(o instanceof DocLuceneScore)) {
      return false;
    }
    try {
      return text().equals(((DocLuceneScore) o).text());

    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public int hashCode() {
    return docContents.hashCode();
  }

  /**
   * @return score of doucmnet, as returned by the lucene search
   */
  @Override
  public double score() {
    return score;
  }

  @Override
  public boolean setScore(double newScore) {
    this.score = newScore;
    return true;
  }

  public Document doc() {
    return doc;
  }

}