package schnell.struct.doc;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface DocID {

  public String docID();

  public void setDocID(String docID);

}
