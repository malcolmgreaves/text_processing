package schnell.struct;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 * @param <T>
 */
public interface StringArrayFormatter<A> {

  public String[] format(A input) throws Exception;
  
}