package schnell.struct.text;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface Offset {
	
	/**
	 * @return the beginning offset
	 */
	public int beg();
	
	/**
	 * @param beg the beginning offset
	 */
	public void setBeg(int beg);
	
	/**
	 * @return the end offset
	 */
	public int end();
	
	/**
	 * @param end the end offset
	 */
	public void setEnd(int end);
	
}