package schnell.struct.text.tag;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface POS {

  public String posTag();
  
  public void setPOSTag(String posTag) throws IllegalArgumentException;
  
}