package schnell.struct.text.tag;

import schnell.struct.text.Text;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class NERTagged
implements Text, NER {
  /* class methods */
  @Override
  public String toString(){
    return Strings.build(word," ",tag);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
    result = prime * result + ((word == null) ? 0 : word.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    
    NERTagged other = (NERTagged) obj;
    if (tag == null) {
      if (other.tag != null)
        return false;
    } else if (!tag.equals(other.tag))
      return false;
    
    if (word == null) {
      if (other.word != null)
        return false;
    } else if (!word.equals(other.word))
      return false;
    
    return true;
  }

  /* constructors */

  public NERTagged(){
    this("","O");
  }
  
  public NERTagged(String word, String tag){
    setText(word);
    setNERTag(tag);
  }

  /* constructors */

  protected String word;
  public String text(){ return word; }
  public void setText(String word){
    Exceptions.checkNonNull(word, "word");
    this.word =  word;
  }

  protected String tag;
  public String nerTag(){ return tag; }
  public void setNERTag(String tag){
    Exceptions.checkNonNull(tag, "NER tag");
    this.tag =  tag;
  } 

}