package schnell.struct.text.tag;

import schnell.struct.text.Text;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class POSTagged
implements Text, POS {
  /* class methods */
  @Override
  public String toString(){
    return Strings.build(word," ",tag);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((tag == null) ? 0 : tag.hashCode());
    result = prime * result + ((word == null) ? 0 : word.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    
    POSTagged other = (POSTagged) obj;
    if (tag == null) {
      if (other.tag != null)
        return false;
    } else if (!tag.equals(other.tag))
      return false;
    
    if (word == null) {
      if (other.word != null)
        return false;
    } else if (!word.equals(other.word))
      return false;
    
    return true;
  }

  /* constructors */

  public POSTagged(){
    this("","O");
  }
  
  public POSTagged(String word, String tag){
    setText(word);
    setPOSTag(tag);
  }

  /* constructors */

  protected String word;
  public String text(){ return word; }
  public void setText(String word){
    Exceptions.checkNonNull(word, "word");
    this.word =  word;
  }

  protected String tag;
  public String posTag() { return tag; }
  public void setPOSTag(String posTag) {
    Exceptions.checkNonNull(posTag);
    this.tag = posTag;
  }

}