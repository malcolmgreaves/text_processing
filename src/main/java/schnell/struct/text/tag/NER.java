package schnell.struct.text.tag;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface NER {
  
  public String nerTag();
  
  public void setNERTag(String ner) throws IllegalArgumentException;

}
