package schnell.struct.text.parse;

import java.util.ArrayList;
import java.util.List;

import org.maltparser.core.symbol.SymbolTable;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.edge.Edge;
import org.maltparser.core.syntaxgraph.node.DependencyNode;
import org.maltparser.core.syntaxgraph.node.Root;

import schnell.process.ConverterExcept;
import schnell.struct.text.TextPosNerDp;
import schnell.struct.text.tok.TokenPosNerDp;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class Dependency2HazyConverter
implements ConverterExcept<DependencyStructure, List<TextPosNerDp>, Exception> {

  @Override
  public List<TextPosNerDp> convert(DependencyStructure input) 
      throws Exception {
    List<TextPosNerDp> tokens = new ArrayList<TextPosNerDp>(input.nDependencyNode());

    for (int index = 1; index <= input.getHighestDependencyNodeIndex(); index++) {
      DependencyNode dpNode = input.getDependencyNode(index);
      if(dpNode == null){
        throw new Exception(String.format("no dependency node for index %d", index));
      }
      if(dpNode.getIndex() != index){
        throw new Exception(String.format("dependency node's index != mapped index (%d, %d)",
            dpNode.getIndex(), index));
      }


      String dependencyLabel = null;
      int dependentIndex = -1;
      if(!dpNode.getClass().equals(Root.class)){
        DependencyNode head = dpNode.getHead();
        Edge depepdnecyEdge = dpNode.getHeadEdge();
        dependentIndex = head.getIndex();

        for(SymbolTable table : depepdnecyEdge.getLabelTypes()){
          if(table.toString().equals("DEPREL 51")){
            dependencyLabel = depepdnecyEdge.getLabelSymbol(table);
            break;
          }
        }
      } else {
        dependencyLabel = "null";
        dependentIndex = 0;
      }
      if(dependentIndex == -1 || dependencyLabel == null){
        throw new Exception("never found a dependency index or a dependency label");
      }

      // get the raw text and part of speech tag from the node
      String rawText = null, pos = null, fileOffset = null;
      for(SymbolTable sTable : dpNode.getLabelTypes()){

        if(sTable.getName().equals("FORM")){
          rawText = dpNode.getLabelSymbol(sTable);
        } else if(sTable.getName().equals("CPOSTAG")){
          pos = dpNode.getLabelSymbol(sTable);
        } else if (sTable.getName().equals("FEATS")){
          fileOffset = dpNode.getLabelSymbol(sTable);
        }
      }
      if(rawText == null || pos == null || fileOffset == null){
        throw new Exception("invalid init, one or all of raw text, pos, or fileOffset is null");
      }

      String ner = fileOffset.split(":")[2];

      TextPosNerDp ht = new TokenPosNerDp(
          rawText,           // token (raw string)
          pos,               // part of speech tag
          ner,               // named entity recognizer tag
          dependencyLabel,   // dependency label
          dependentIndex);   // dependency head index
      tokens.add(ht);
    }

    return tokens;
  }

}