package schnell.struct.text.parse;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface DepParse {

  public String depParseLabel();
  
  public void setDepParseLabel(String depParseLabel) throws IllegalArgumentException;
  
  public int dependencyIndex();
  
  public void setDependencyIndex(int depdencyIndex) throws IllegalArgumentException;

}