package schnell.struct.text;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface Text {
  
  public String text();
  
  public void setText(String text) throws IllegalArgumentException;
  
}