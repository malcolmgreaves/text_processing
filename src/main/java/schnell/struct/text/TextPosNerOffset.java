package schnell.struct.text;

import schnell.struct.text.tag.NER;
import schnell.struct.text.tag.POS;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public interface TextPosNerOffset
extends Text, NER, POS, Offset {

}
