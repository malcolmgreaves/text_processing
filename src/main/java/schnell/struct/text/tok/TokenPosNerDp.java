package schnell.struct.text.tok;

import schnell.struct.text.TextPosNerDp;
import schnell.util.Strings;
import schnell.util.except.Exceptions;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class TokenPosNerDp
	extends Token
	implements TextPosNerDp {
	/* methods */

	/* static members */
	static final protected String UNSET = "";
	protected String posTag;
	protected String nerTag;
	protected String depParseLabel;
	protected int depHeadIndex;

	/* constructors */
	public TokenPosNerDp() {
		this(UNSET, UNSET, UNSET, UNSET, -1);
	}

  /* fields */

	// FIXME fucking args
	public TokenPosNerDp(String token, String posTag, String nerTag,
	                     String depParseLabel, int dependecyIndex) {
		super(token);
		setPOSTag(posTag);
		setNERTag(nerTag);
		setDepParseLabel(depParseLabel);
		setDependencyIndex(dependecyIndex);
	}

	public TokenPosNerDp(TextPosNerDp other) {
		this(other.text(), other.posTag(), other.nerTag(),
			other.depParseLabel(), other.dependencyIndex());
	}

	@Override
	public String toString() {
		return Strings.build(super.toString(), "/", posTag, "/", nerTag, "/",
			depHeadIndex, "/", depParseLabel);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + depHeadIndex;
		result = prime * result
			+ ((depParseLabel == null) ? 0 : depParseLabel.hashCode());
		result = prime * result + ((nerTag == null) ? 0 : nerTag.hashCode());
		result = prime * result + ((posTag == null) ? 0 : posTag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenPosNerDp other = (TokenPosNerDp) obj;

		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;

		if (depHeadIndex != other.depHeadIndex)
			return false;
		if (depParseLabel == null) {
			if (other.depParseLabel != null)
				return false;
		} else if (!depParseLabel.equals(other.depParseLabel))
			return false;

		if (nerTag == null) {
			if (other.nerTag != null)
				return false;
		} else if (!nerTag.equals(other.nerTag))
			return false;

		if (posTag == null) {
			if (other.posTag != null)
				return false;
		} else if (!posTag.equals(other.posTag))
			return false;

		return true;
	}

	public String posTag() {
		return posTag;
	}

	public void setPOSTag(String pos)
		throws IllegalArgumentException {
		Exceptions.checkNonNull(pos, "part of speech tag");
		this.posTag = pos;
	}

	public String nerTag() {
		return nerTag;
	}

	public void setNERTag(String nerTag) {
		Exceptions.checkNonNull(nerTag, "NER tag");
		this.nerTag = nerTag;
	}

	public String depParseLabel() {
		return depParseLabel;
	}

	public void setDepParseLabel(String depLabel)
		throws IllegalArgumentException {
		Exceptions.checkNonNull(depLabel, "dependency label");
		this.depParseLabel = depLabel;
	}

	public int dependencyIndex() {
		return depHeadIndex;
	}

	public void setDependencyIndex(int depHeadIndex) {
		this.depHeadIndex = depHeadIndex;
	}

}