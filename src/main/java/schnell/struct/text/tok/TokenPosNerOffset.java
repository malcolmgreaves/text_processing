package schnell.struct.text.tok;

import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.TextPosNerOffset;
import schnell.util.Strings;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class TokenPosNerOffset
extends TokenPosNer 
implements TextPosNerOffset {
	/* methods */
	
	public String toString(){
		return Strings.build(super.toString(),"(",beg,",",end,")");
	}
	
	/* constructors */
	
	public TokenPosNerOffset(){
		super();
		setBeg(-1);
		setEnd(-1);
  }

  public TokenPosNerOffset(String token, String posTag, String nerTag, int beg, int end){
    super(token,posTag,nerTag);
    setBeg(beg);
    setEnd(end);
  }

  public TokenPosNerOffset(TextPosNerOffset other){
    this(other.text(),other.posTag(),other.nerTag(),other.beg(),other.end());
  }
  
  public TokenPosNerOffset(TextPosNerDpOffset other){
    this(other.text(),other.posTag(),other.nerTag(),other.beg(),other.end());
  }

	
	/* fields */

  protected int beg;
	@Override
	public int beg() { return beg; }
	@Override
	public void setBeg(int beg) { this.beg = beg; }

	protected int end;
	@Override
	public int end() { return end; }
	@Override
	public void setEnd(int end) { this.end = end; }

}