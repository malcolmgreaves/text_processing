package schnell.struct.text.tok;

import schnell.struct.text.TextPosNer;
import schnell.util.Strings;
import schnell.util.except.Exceptions;


/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class TokenPosNer
extends Token
implements TextPosNer {
  /* methods */
  
  @Override
  public String toString(){
    return Strings.build(super.toString(), "/", pos, "/", nerTag);
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((nerTag == null) ? 0 : nerTag.hashCode());
    result = prime * result + ((pos == null) ? 0 : pos.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TokenPosNer other = (TokenPosNer) obj;
    
    if (text == null) {
      if (other.text != null)
        return false;
    } else if (!text.equals(other.text))
      return false;
    
    if (nerTag == null) {
      if (other.nerTag != null)
        return false;
    } else if (!nerTag.equals(other.nerTag))
      return false;
    
    if (pos == null) {
      if (other.pos != null)
        return false;
    } else if (!pos.equals(other.pos))
      return false;
    
    return true;
  }

  /* constructors */
  
  public TokenPosNer(){
    this("", "", "");
  }

  public TokenPosNer(String token, String posTag, String nerTag){
    super(token);
    setPOSTag(posTag);
    setNERTag(nerTag);
  }

  public TokenPosNer(TextPosNer other){
    this(other.text(), other.posTag(), other.nerTag());
  }

  /* fields */

  protected String pos;
  public String posTag() { return pos; }
  public void setPOSTag(String pos)
      throws IllegalArgumentException {
    Exceptions.checkNonNull(pos, "part of speech tag");
    this.pos = pos;
  }
  
  protected String nerTag;
  public String nerTag(){ return nerTag; }
  public void setNERTag(String nerTag){
    Exceptions.checkNonNull(nerTag, "NER tag");
    this.nerTag = nerTag;
  }

}