package schnell.struct.text.tok;

import schnell.struct.text.Text;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class Token
implements Text {
  /* class methods */
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((text == null) ? 0 : text.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Token other = (Token) obj;
    if (text == null) {
      if (other.text != null)
        return false;
    } else if (!text.equals(other.text))
      return false;
    return true;
  }
  
  @Override
  public String toString(){
    return text;
  }

  /* constructors */

  public Token(Token other){
    Exceptions.checkNonNull(other, "other token to copy");
    setText(other.text);
  }

  public Token(){
    this("");
  }

  public Token(String token){
    setText(token);
  }

  /* class fields */
  
  protected String text;
  public String text() { return text; }
  public void setText(String token)
      throws IllegalArgumentException {
    Exceptions.checkNonNull(token, "token");
    this.text = token;
  }

}