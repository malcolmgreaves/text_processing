package schnell.struct.text.tok;

import java.util.List;

import schnell.util.func.Func;
import schnell.struct.text.Text;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class TokUtil {
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
  static public String toOriginalSentence(List tokens){
    return new _toOriginalSentence().evaluate(tokens);
  }
  
  static private class _toOriginalSentence<E extends Text>
  implements Func.Binary<List<E>, String>{
    @Override
    public String evaluate(List<E> tokens) {
      Exceptions.checkNonNull(tokens, "tokens");
      StringBuilder sb = new StringBuilder();
      for(Text tok : tokens){
        sb.append(tok.text()).append(" ");
      }
      return sb.toString().trim();
    }  
  }
  
  @SuppressWarnings({ "unchecked", "rawtypes" })
  static public String toTokenIndexedSentence(List tokens){
    return new _toTokenIndexedSentence().evaluate(tokens);
  }
  
  static private class _toTokenIndexedSentence<E extends Text>
  implements Func.Binary<List<E>, String>{
    @Override
    public String evaluate(List<E> tokens) {
      Exceptions.checkNonNull(tokens, "tokens");
      StringBuilder sb = new StringBuilder();
      int ii = 1;
      for(Text tok : tokens){
        sb.append("[").append(ii++).append("] ").append(tok.text()).append(" ");
      }
      return sb.toString().trim();
    }  
  }
  
}