package schnell.struct.text.tok;

import schnell.struct.text.TextPosNerDp;
import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.TextPosNerOffset;
import schnell.util.Strings;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class TokenPosNerDpOffset
extends TokenPosNerDp
implements TextPosNerDpOffset {
	/* methods */

	@Override
	public String toString(){
		return Strings.build(super.toString(),"(",beg,",",end,")");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + beg;
		result = prime * result + end;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		TokenPosNerDpOffset other = (TokenPosNerDpOffset) obj;
		if(!super.equals(other)) return false;
		if (beg != other.beg) return false;
		if (end != other.end) return false;
		return true;
	}

	/* constructors */

	public TokenPosNerDpOffset(){
		super();
		setBeg(-1);
		setEnd(-1);
	}

	public TokenPosNerDpOffset(TextPosNerDpOffset tok){
		this(tok.text(), tok.posTag(), tok.nerTag(), tok.depParseLabel(),
				tok.dependencyIndex(), tok.beg(), tok.end());
	}

	public TokenPosNerDpOffset(TextPosNerDp tok, int beg, int end){
		this(tok.text(), tok.posTag(), tok.nerTag(), tok.depParseLabel(),
				tok.dependencyIndex(), beg, end);
	}

	public TokenPosNerDpOffset(String text, String posTag, String nerTag, 
			String depParseLabel, int depHeadIndex, int beg, int end){
		super(text, posTag, nerTag, depParseLabel, depHeadIndex);
		setBeg(beg);
		setEnd(end);
	}

	/* fields */

	public TokenPosNerDpOffset(TextPosNerOffset tok) {
		this(tok.text(),tok.posTag(),tok.nerTag(),
				"",0,
				tok.beg(),tok.end());
	}

	protected int beg;
	@Override
	public int beg(){ return beg; }
	@Override
	public void setBeg(int beg){
		this.beg = beg;
	}

	protected int end;
	@Override
	public int end(){ return end; }
	@Override
	public void setEnd(int end){
		this.end = end;
	}

}