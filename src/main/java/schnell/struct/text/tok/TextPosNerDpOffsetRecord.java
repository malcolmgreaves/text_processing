package schnell.struct.text.tok;

import java.util.List;

import schnell.struct.text.TextPosNerDpOffset;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class TextPosNerDpOffsetRecord {
  /* methods */
  
  @Override
  public String toString(){
    StringBuilder sb = new StringBuilder();
    for(int ii = 0; ii < toklist.size(); ii++){
      sb.append("[").append(ii+1).append("] ")
      .append(toklist.get(ii))
      .append("\n");
    }
    return sb.toString().trim();
  }
  
  /* constructors */
  
  public TextPosNerDpOffsetRecord(List<TextPosNerDpOffset> tokenList){
    setTokenList(tokenList);
  }
  
  /* fields */
  
  protected List<TextPosNerDpOffset> toklist;
  public List<TextPosNerDpOffset> tokenList(){ return toklist; }
  public void setTokenList(List<TextPosNerDpOffset> tokenList){
    Exceptions.checkNonNull(tokenList, "token list");
    this.toklist = tokenList;
  }
  
}
