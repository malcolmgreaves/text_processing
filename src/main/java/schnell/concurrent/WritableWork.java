package schnell.concurrent;

import java.io.BufferedWriter;
import java.util.List;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public class WritableWork<T> {
	final public boolean isDone;
	final public BufferedWriter writer;
	final public List<T> todo;

	public WritableWork(boolean isDone, BufferedWriter w, List<T> todo) {
		this.isDone = isDone;
		this.writer = w;
		this.todo = todo;
	}
}