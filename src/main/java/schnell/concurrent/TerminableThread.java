package schnell.concurrent;

import org.apache.log4j.Logger;
import schnell.util.except.Exceptions;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public abstract class TerminableThread<T> extends Thread {
	static final public int QUEUE_LIMIT = 100;
	static protected Logger Log = Logger.getLogger(TerminableThread.class);
	// fields
	final private T STOP = stopInstance();
	final private LinkedBlockingQueue<T> queue;
	final private LinkedBlockingQueue<Object> hasStopped;

	public TerminableThread(int queueLimit) {
		this.queue = new LinkedBlockingQueue<T>(queueLimit < 1 ? QUEUE_LIMIT : queueLimit);
		this.hasStopped = new LinkedBlockingQueue<Object>(1);
	}

	@Override
	public void run() {
		T work;
		while (true) {
			try {
				work = queue.take();
				if (work == STOP) {
					finishUp();
					hasStopped.put(new Object());
					return;
				}
				action(work);

			} catch (InterruptedException e) {
				Log.error("couldn't get work from queue", e);
			}
		}
	}

	protected abstract void action(T work);

	protected abstract void finishUp();

	/**
	 * Submits work asynchronously.
	 *
	 * @param work non null
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	public void submit(T work) throws InterruptedException {
		Exceptions.checkNonNull(work, "work to do");
		queue.put(work);
	}

	public void terminate() throws InterruptedException {
		queue.put(STOP);
		hasStopped.take();
		queue.clear();
	}

	protected abstract T stopInstance();

}