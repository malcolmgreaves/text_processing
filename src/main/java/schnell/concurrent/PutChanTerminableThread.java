package schnell.concurrent;

import org.apache.log4j.Logger;
import schnell.struct.ds.PutChannel;
import schnell.util.except.Exceptions;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public abstract class PutChanTerminableThread<IN, OUT> extends Thread {
	static final public int QUEUE_LIMIT = 2;
	static protected Logger Log = Logger.getLogger(PutChanTerminableThread.class);
	// fields
	final private IN STOP = stopInstance();
	final private OUT ALL_CLEAR = allClearInstance();
	final private LinkedBlockingQueue<IN> queue;
	final private LinkedBlockingQueue<Object> hasStopped;
	protected PutChannel<OUT> outputChannel;

	public PutChanTerminableThread(int queueLimit, PutChannel<OUT> outputChannel) {
		this.queue = new LinkedBlockingQueue<IN>(queueLimit < 1 ? QUEUE_LIMIT : queueLimit);
		Exceptions.checkNonNull(outputChannel, "output channel");
		this.outputChannel = outputChannel;
		this.hasStopped = new LinkedBlockingQueue<Object>(1);
	}

	/**
	 * Do not call sumbit(IN) if the Thread hasn't been started.
	 * I.e. dont' call terminate() then submit(IN). One must call
	 * start() before submit(IN) is called.
	 */
	public void submit(IN work) throws InterruptedException {
		Exceptions.checkNonNull(work, "work to do");
		queue.put(work);
	}

	@Override
	public void run() {
		IN work;
		while (true) {
			try {
				work = queue.take();
				if (work == STOP) {
					finishUp();
					hasStopped.put(new Object());
					return;
				}
				outputChannel.put(action(work));

			} catch (InterruptedException e) {
				Log.error("couldn't get work from queue", e);
			}
		}
	}

	protected abstract void finishUp();

	protected abstract OUT action(IN input);

	public void terminate() throws InterruptedException {
		queue.put(STOP);
		hasStopped.take();
		queue.clear();
	}

	protected abstract IN stopInstance();

	protected abstract OUT allClearInstance();

}