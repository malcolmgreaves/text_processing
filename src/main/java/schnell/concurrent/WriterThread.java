package schnell.concurrent;

import org.apache.log4j.Logger;
import schnell.data.write.Writer;
import schnell.util.Strings;

import java.io.IOException;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public class WriterThread<T> extends TerminableThread<WritableWork<T>> {
	static protected Logger Log = Logger.getLogger(WriterThread.class);
	// fields
	final public Writer<T> writer;
	volatile protected long nWrote = 0, nUpdated = 0;

	public WriterThread(int queueLimit, Writer<T> writer) {
		super(queueLimit);
		this.writer = writer;
	}

	@Override
	protected void action(final WritableWork<T> work) {
		// use the supplied writer to write out all of the completed work
		for (T p : work.todo) {
			try {
				writer.write(work.writer, p);

			} catch (IOException e) {
				Log.error(Strings.build("couldn't write: ", p), e);
			}
		}

		nWrote += work.todo.size();
		if (nUpdated % 10 == 0) {
			Log.info(Strings.build("wrote ", nWrote, " items"));
		}
		nUpdated++;

		if (work.isDone) {
			try {
				work.writer.close();
				Log.info("completed writing, writer closed");

			} catch (IOException e) {
				Log.error("writer thread failed to close buffered writer", e);
			}
		}
	}

	@Override
	protected void finishUp() { /* nothing */ }

	@Override
	protected WritableWork<T> stopInstance() {
		return new WritableWork<T>(false, null, null);
	}

}