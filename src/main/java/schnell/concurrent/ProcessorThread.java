package schnell.concurrent;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.maltparser.core.exception.MaltChainedException;
import schnell.pipeline.test.ConcurrentMakeSlotfillInstances;
import schnell.pipeline.test.ProcessedQuery;
import schnell.process.text.TextProcessor;
import schnell.struct.IdedChunkedParsed;
import schnell.struct.doc.ScoredDocument;
import schnell.struct.ds.PutChannel;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public class ProcessorThread
    extends PutChanTerminableThread<ConcurrentMakeSlotfillInstances.DocumentsQuery, ProcessedQuery> {

  static protected Logger log = Logger.getLogger(ProcessorThread.class);
  // fields
  final protected TextProcessor textProcessor;

  public ProcessorThread(int queueLimit, Configuration conf,
                         PutChannel<ProcessedQuery> outputChannel
                        ) throws MaltChainedException, IOException, ClassNotFoundException {
    super(queueLimit, outputChannel);
    Exceptions.checkNonNull(conf, "configuration to make TextProcessor with");
    this.textProcessor = TextProcessor.make(conf, log);
  }

  @Override
  protected void finishUp() { /* nothing */ }

  @Override
  protected ProcessedQuery action(final ConcurrentMakeSlotfillInstances.DocumentsQuery input) {
    final ProcessedQuery pq = processQuery(input);
    Log.info(Strings.build("processed and extracted query ID: ", input.name,
                           " resulting in ", pq.processedSentences.size(), " processed sentences"));
    return pq;
  }

  protected ProcessedQuery processQuery(final ConcurrentMakeSlotfillInstances.DocumentsQuery input) {
    final ProcessedQuery pq = new ProcessedQuery(
        input.name,
        new ArrayList<IdedChunkedParsed>(input.documents.size())
    );


    for (int si = 0; si < input.documents.size(); si++) {
      final ScoredDocument sd = input.documents.get(si);
      try {
        pq.processedSentences.add(new IdedChunkedParsed(
            sd.docsentID(),
            // perform POS and NE tagging
            // as well as dependency parsing
            // and nounphrase chunking
            textProcessor.process(sd.text()))
                                 );

        if (si % 20550 == 0) {
          Log.info(Strings.build("[", getName(), "] parsed ", si == 0 ? 1 : si, " sentences"));
        }
      } catch (Exception e) {
        log.warn(Strings.build("[skip] failed to text process document ID: ", sd.docID(),
                               "\n", sd.text()), e);
      }
    }

    return pq;
  }

  @Override
  protected ConcurrentMakeSlotfillInstances.DocumentsQuery stopInstance() {
    return new ConcurrentMakeSlotfillInstances.DocumentsQuery("done", new ArrayList<ScoredDocument>(0));
  }

  @Override
  protected ProcessedQuery allClearInstance() {
    return new ProcessedQuery("done", new ArrayList<IdedChunkedParsed>(0));
  }
}