package schnell.data;

import java.io.IOException;
import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.IndexableFieldType;
import org.apache.lucene.util.BytesRef;


import schnell.data.index.search.Querier;
import schnell.process.text.TextProcessor;
import schnell.process.text.chunk.PhraseChunker;
import schnell.process.text.parse.MaltParser;
import schnell.process.text.tag.StanfordNERTagger;
import schnell.process.text.tag.StanfordPOSTagger;
import schnell.util.func.Func;
import schnell.struct.doc.DocLuceneScore;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class TestingData {

  static final public String
  TEST_SENTENCE_1 = "John kicked the bucket.";
  
  static final public Func.Unary<DocLuceneScore>
  TEST_SENTENCE_DOC_1 = new Func.Unary<DocLuceneScore>(){
    @Override
    public DocLuceneScore evaluate() {
      Document lDoc = new Document();
      
      lDoc.add(new IndexableField(){
        @Override public String name() { return Querier.DOCSENTID; }
        @Override public IndexableFieldType fieldType() {  return null; }
        @Override public float boost() { return 0; }
        @Override public BytesRef binaryValue() { return null; }
        @Override public String stringValue() { return "TK421"; }
        @Override public Reader readerValue() { return null; }
        @Override public Number numericValue() { return null; }
        @Override public TokenStream tokenStream(Analyzer analyzer) throws IOException { return null; }
      });
      
      lDoc.add(new IndexableField(){
        @Override public String name() { return Querier.CONTENTS; }
        @Override public IndexableFieldType fieldType() {  return null; }
        @Override public float boost() { return 0; }
        @Override public BytesRef binaryValue() { return null; }
        @Override public String stringValue() { return TestingData.TEST_SENTENCE_1; }
        @Override public Reader readerValue() { return null; }
        @Override public Number numericValue() { return null; }
        @Override public TokenStream tokenStream(Analyzer analyzer) throws IOException { return null; }
      });
      
      return new DocLuceneScore(1.0, lDoc);
    }
  };
  
  static final public Func.Binary<PhraseChunker,TextProcessor>
  DOC_PROCESSOR = new Func.Binary<PhraseChunker,TextProcessor>(){
    @Override
    public TextProcessor evaluate(PhraseChunker chunker) {
      try{
        return new TextProcessor(
            StanfordPOSTagger.make(),
            StanfordNERTagger.make(),
            MaltParser.make(),
            chunker);
      }catch(Exception e){
        throw new RuntimeException(e);
      }
    }
  };
  
  
}