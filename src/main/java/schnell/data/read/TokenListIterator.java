package schnell.data.read;

import com.google.common.collect.Lists;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import schnell.data.io.IO;
import schnell.struct.text.TextPosNerDpOffset;
import schnell.struct.text.tok.TokenPosNerDpOffset;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *         j
 */
public class TokenListIterator
	implements Iterator<List<TextPosNerDpOffset>>, ReaderIterator<List<TextPosNerDpOffset>> {
	static protected Logger Log = Logger.getLogger(TokenListIterator.class);

	static {
		Log.setLevel(Level.INFO);
	}

	/* methods */
	final protected BufferedReader br;
	protected List<TextPosNerDpOffset> tokList;
	private boolean isClosed;

  /* helper functions */

	public TokenListIterator(File data)
		throws IllegalArgumentException, IOException {
		Exceptions.checkFileExists(data, "data");
		this.br = IO.reader(data);
	}

	public TokenListIterator(BufferedReader r)
		throws IllegalArgumentException, IOException {
		Exceptions.checkBufferReady(r, "reader");
		this.br = r;
	}

	/**
	 * Input line format:
	 * index text lemmatized_text text_pos lemmatized_text_pos file_offset_and_ner_tag dependncy_index dependency_label _ _
	 * <p/>
	 * Example:
	 * 1 The the DT DT clueweb09-en0043-21-00000:14:O:1559:1562 2 det _ _
	 *
	 * @param line line to construct a token from
	 * @return a token constructed from the information in the line
	 * @throws IllegalArgumentException if the line is empty, null, or in an incorrect format
	 */
	static public TextPosNerDpOffset parseToken(String line)
		throws IllegalArgumentException {

		Exceptions.checkNonNullNonEmpty(line, "input line to parse from");
		String[] fields = Strings.tabSplit(line);
		if (fields.length < 8) {
			throw new IllegalArgumentException(String.format(
				"line doesn't have at least 8 fields: %s", line));
		}

		String[] offsetInfo = fields[5].split(":");

		return new TokenPosNerDpOffset(
			fields[1],                       // actual token, not lemmatized one
			fields[3],                       // part of speech tag
			offsetInfo[2],                   // ner tag
			fields[7],                       // dependency label
			Integer.parseInt(fields[6]),     // head index
			Integer.parseInt(offsetInfo[3]), // beginning offset
			Integer.parseInt(offsetInfo[4])  // end offset
		);
	}

	@Override
	public boolean hasNext() {
		if (isClosed) {
			// reader is closed
			return false;
		}
		TextPosNerDpOffset tok;
		List<TextPosNerDpOffset> curRecord = Lists.newArrayList();
		try {
			for (String line; (line = br.readLine()) != null; ) {
				line = line.trim();
				if (line.length() > 0) {
					// continue parsing record
					tok = parseToken(line);
					if (Log.isDebugEnabled()) {
						Log.debug(Strings.build("\t", line, " --> ", tok));
					}
					curRecord.add(tok);

				} else {
					// record delimiter
					tokList = curRecord;
					if (Log.isDebugEnabled()) {
						Log.debug(Strings.build("READ:\t", curRecord, "\n"));
					}
					return true;
				}
			}
		} catch (IOException readerException) {
			// problem reading line from file; cannot recover
			Log.error(Strings.build("Error attempting to read from file: ",
				Exceptions.toString(readerException)));
		}

		tokList = null;
		isClosed = true;
		try {
			br.close();
		} catch (IOException closingException) {
			// ignore this exception: we're returning false anyway
			Log.warn(Strings.build("Error attempting to close reader:\n",
				Exceptions.toString(closingException)));
		}
		return false;
	}

	@Override
	public List<TextPosNerDpOffset> next()
		throws NoSuchElementException {
		if (tokList == null) {
			throw new NoSuchElementException(
				"Token list is empty, did you makeBloomFilter sure hasNext() == true?");
		}
		if (br == null) {
			throw new NoSuchElementException(
				"Finished reading through file, no more elements left.");
		}
		List<TextPosNerDpOffset> next = tokList;
		return next;
	}

  /* constructors */

	@Override
	public void remove() {
		throw new UnsupportedOperationException(
			"remove is undefined for a token list iterator");
	}

	@Override
	public Iterator<List<TextPosNerDpOffset>> read(File data)
		throws IOException {
		return new TokenListIterator(data);
	}

	@Override
	public Iterator<List<TextPosNerDpOffset>> read(final BufferedReader data)
		throws IOException {
		return new TokenListIterator(data);

	}


}