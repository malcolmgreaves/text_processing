package schnell.data.read;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Maps;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import schnell.data.index.search.KBPQuery;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author William Wang
 */
public class QueryParser {

	public static List<KBPQuery> parseXML(File file)
		throws DocumentException {
		List<KBPQuery> queries = Lists.newArrayList();
		SAXReader reader = new SAXReader();
		Document document = reader.read(file);
		Element root = document.getRootElement();

		for (Iterator<Element> i = root.elementIterator(); i.hasNext(); ) {
			Element element = i.next();
			String queryName = element.attribute(0).getValue();
			Map<String, String> queryAttributes = Maps.newHashMap();

			for (Iterator<Element> j = element.elementIterator(); j.hasNext(); ) {
				Element attribute = j.next();
				queryAttributes.put(attribute.getName(), attribute.getStringValue());
			}
			try {
				queries.add(new KBPQuery(queryName, queryAttributes));
			} catch (Exception e) {
				System.err.println(Strings.build("Could not add query: ", queryName,
					"\nattributes: ", queryAttributes, "\nError:\n", Exceptions.toString(e)));
			}
		}
		return queries;
	}

}