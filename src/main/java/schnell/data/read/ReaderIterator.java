package schnell.data.read;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * @param <F>
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public interface ReaderIterator<T> {

	/**
	 * @param data
	 * @return
	 */
	public Iterator<T> read(File data) throws IOException;

	/**
	 * @param data
	 * @return
	 */
	public Iterator<T> read(BufferedReader data) throws IOException;

}