package schnell.data.index.search;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import schnell.struct.doc.DocLuceneScore;
import schnell.util.except.Exceptions;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class Querier {
  /* class methods */

	@SuppressWarnings("deprecation")
	static final public Version LUCENE_VERSION = Version.LUCENE_40;
	/**
	 * Fields specific to the search indices we're using.
	 */
	static final public String
		CONTENTS = "contents",   // document's contents (i.e. the actual sentence)
		DOCSENTID = "docsentid", // the document ID + the sentence ID (within document)
		DOCID = "docid",         // the document ID
		NER = "ner",             // the NER tagged text
		HEADLINE = "headline",   // the newswire headline that the document comes from
		BEG = "beg",             // marks the beginning of the document's contents
		END = "end";             // marks the end of the document's contents
	static protected Logger log = Logger.getLogger(Querier.class);
	protected Analyzer analyzer;
	protected IndexWriterConfig config;
	/* helper functions for methods */
	protected QueryParser nameParser;
	/* constructors */
	protected QueryParser docSentIDParser;
	/* class fields */
	protected QueryParser docIDParser;
	protected QueryParser nerParser;
	protected Directory index;

	/**
	 * @param indexLocation
	 * @throws IOException
	 */
	public Querier(Directory indexLocation)
		throws IOException {
		this.analyzer = new StandardAnalyzer(LUCENE_VERSION);
		this.config = new IndexWriterConfig(LUCENE_VERSION, analyzer);
		this.nameParser = new QueryParser(LUCENE_VERSION, CONTENTS, analyzer);
		this.docSentIDParser = new QueryParser(LUCENE_VERSION, DOCSENTID, analyzer);
		this.docIDParser = new QueryParser(LUCENE_VERSION, DOCSENTID, analyzer);
		this.nerParser = new QueryParser(LUCENE_VERSION, NER, analyzer);
		// store the index in memory
		//    this.index = new RAMDirectory();
		// an index on disk, use this instead: this.index = FSDirectiry.open("LOCATION/ON/DISK");
		this.index = indexLocation;
	}

	/**
	 * @param isearcher
	 * @param q
	 * @param maxDocs
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	static public DocLuceneScore[] query(IndexSearcher isearcher, Query q, int maxDocs)
		throws ParseException, IOException, InterruptedException, ExecutionException {
		Exceptions.checkNonNull(isearcher, "index searcher");
		Exceptions.checkNonNull(q, "query");
		Exceptions.checkNonNegative(maxDocs, "maximum # of documents to return");

		ScoreDoc[] hits = isearcher.search(q, null, maxDocs).scoreDocs;
		ScoreDoc sdoc;

		// get the document contents for each hit
		// perform this computation in parallel
		DocLuceneScore[] scoredLuceneDocs = new DocLuceneScore[hits.length];
		for (int ii = 0; ii < hits.length; ii++) {
			sdoc = hits[ii];
			scoredLuceneDocs[ii] = new DocLuceneScore(sdoc.score, isearcher.doc(sdoc.doc));
		}

		return scoredLuceneDocs;
	}

	static public Iterator<Document> iterateOverAllDocs(final IndexReader reader) {
		Exceptions.checkNonNull(reader, "index reader to iterate over");

		return new Iterator<Document>() {

			private Document doc;
			private int di = 0;
			private boolean closed = false;

			@Override
			public boolean hasNext() {
				if (!closed) {
					while (di < reader.maxDoc()) {
//					if (reader.isDeleted(i)) continue;
						try {
							doc = reader.document(di);
							di++;
							return true;

						} catch (IOException e) {
							log.warn("[skip] error getting document # " + di, e);
						}
					}

					try {
						closed = true;
						reader.close();
					} catch (IOException e) {
						log.error("error closing index reader after hasNext() will always return false", e);
					}
				}
				return false;
			}

			@Override
			public Document next() {
				return doc;
			}

			@Override
			public void remove() {
				throw new UnsupportedClassVersionError("remove on Lucene document collection");
			}
		};
	}

	public Iterator<Document> iterateOverAllDocs()
		throws IOException {
		return Querier.iterateOverAllDocs(
			new IndexSearcher(DirectoryReader.open(index)).getIndexReader()
		);
	}

	/**
	 * @param term
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public DocLuceneScore[] queryName(String term, int maxDocs)
		throws IOException, ParseException, InterruptedException, ExecutionException {
		Exceptions.checkNonNull(term, "term to serach for");
		DirectoryReader ireader = DirectoryReader.open(index);
		DocLuceneScore[] hits = query(new IndexSearcher(ireader), nameParser.parse(term), maxDocs);
		ireader.close();
		return hits;
	}

	public DocLuceneScore[] queryDocID(String docID, int maxDocs)
		throws IOException, ParseException, InterruptedException, ExecutionException {
		Exceptions.checkNonNull(docID, "doc ID to search for");
		DirectoryReader ireader = DirectoryReader.open(index);
		DocLuceneScore[] hits = query(new IndexSearcher(ireader), docIDParser.parse(docID), maxDocs);
		ireader.close();
		return hits;
	}

	public DocLuceneScore[] queryNameAndAnswer(String queryName, String answer, int maxDocs)
		throws IOException, ParseException, InterruptedException, ExecutionException {
		Exceptions.checkNonNull(queryName, "query text to search for");
		Exceptions.checkNonNull(answer, "answer to search for");
		BooleanQuery q = new BooleanQuery();
		q.add(nameParser.parse(queryName), BooleanClause.Occur.MUST);
		q.add(nameParser.parse(answer), BooleanClause.Occur.MUST);
		DirectoryReader ireader = DirectoryReader.open(index);
		DocLuceneScore[] hits = query(new IndexSearcher(ireader), q, maxDocs);
		ireader.close();
		return hits;
	}

	public QueryParser nameQueryParser() {
		return nameParser;
	}

	public QueryParser docSentIDQueryParser() {
		return docSentIDParser;
	}

	public QueryParser docIDQueryParser() {
		return docIDParser;
	}

  /* static objects */

	public QueryParser nerQueryParser() {
		return nerParser;
	}

	public enum Type {
		NEWSWIRE,
		DISCUSSION,
		WEB
	}

}
