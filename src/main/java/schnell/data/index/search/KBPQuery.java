package schnell.data.index.search;

import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.util.Map;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class KBPQuery {

	final public String queryName, queryText, docID, docSentID, type, ignore,
		nerTag, beg, end, nodeID;
	final public Querier.Type qtype;

	// FIXME add immutable list for ignore

	public KBPQuery(String queryName, String queryText, String docID, String docSentID, String type,
	                String ignore, String nerTag, String beg, String end, String nodeID,
	                Querier.Type qtype) {
		Exceptions.checkNonNullNonEmpty(queryName, "queryName");
		Exceptions.checkNonNullNonEmpty(queryText, "text");
		Exceptions.checkNonNullNonEmpty(docID, "docsentID");
		Exceptions.checkNonNullNonEmpty(type, "type");
		Exceptions.checkNonNullNonEmpty(beg, "beg");
		Exceptions.checkNonNullNonEmpty(end, "end");
		Exceptions.checkNonNullNonEmpty(nodeID, "nodeID");
		Exceptions.checkNonNull(qtype, "query type");
		this.queryName = queryName;
		this.queryText = queryText;
		this.docID = docID;
		this.docSentID = docSentID;
		this.type = type;
		this.ignore = ignore;
		this.nerTag = nerTag;
		this.beg = beg;
		this.end = end;
		this.nodeID = nodeID;
		this.qtype = qtype;
	}

	public KBPQuery(String queryName, Map<String, String> attributes) {
		this(queryName, attributes.get("name"), attributes.get("docid"),
			attributes.containsKey("docsentid") ? attributes.get("docsentid") : null,
			attributes.get("enttype"), attributes.get("ignore"),
			attributes.containsKey("ner") ? attributes.get("ner") : null,
			attributes.get("beg"), attributes.get("end"), attributes.get("nodeid"),
			LuceneQuerierSelector.name2querierType(attributes.get("docid")));
	}

	@Override
	public String toString() {
		return Strings.build(
			"Query Name:  ", queryName, "\n",
			"Text:        ", queryText, "\n",
			"Doc ID:      ", docID, "\n",
			"DocSent ID:  ", Strings.safe(docSentID), "\n",
			"Query Type:  ", type, "\n",
			"Ignore:      ", Strings.safe(ignore), "\n",
			"NER Tag:     ", Strings.safe(nerTag), "\n",
			"Beg:         ", beg, "\n",
			"End:         ", end, "\n",
			"Node ID:     ", nodeID
		);
	}

	public KBPQuery copyDeep() {
		return new KBPQuery(queryName, queryText, docID, docSentID, type, ignore,
			nerTag, beg, end, nodeID, qtype);
	}

}