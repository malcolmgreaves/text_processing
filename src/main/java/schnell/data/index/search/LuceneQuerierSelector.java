package schnell.data.index.search;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.apache.lucene.store.FSDirectory;
import schnell.run.ReadConfigFile;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.File;
import java.io.IOException;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public class LuceneQuerierSelector
	implements QuerierSelector {

	static protected Logger Log = Logger.getLogger(LuceneQuerierSelector.class);
	protected Querier newswire, web, discussion;

	public LuceneQuerierSelector(Querier newswire, Querier web, Querier discussion) {
		Exceptions.checkNonNull(newswire, "newswire index");
		Exceptions.checkNonNull(discussion, "discussion index");
		Exceptions.checkNonNull(web, "web index");
		this.newswire = newswire;
		this.web = web;
		this.discussion = discussion;
	}

	static public Querier.Type name2querierType(String qdocid) {
		Exceptions.checkNonNull(qdocid, "search index name from prefix of query doc id");
		if (qdocid.startsWith("eng-")) {
			return Querier.Type.WEB;

		} else if (qdocid.startsWith("bolt-")) {
			return Querier.Type.DISCUSSION;
		}
		return Querier.Type.NEWSWIRE;
	}

	static public LuceneQuerierSelector make(Configuration conf, Logger log)
		throws IOException {
		File newswireIndex = new File(conf.get(ReadConfigFile.NEWSWIRE_INDEX));
		File webIndex = new File(conf.get(ReadConfigFile.WEB_INDEX));
		File discussionIndex = new File(conf.get(ReadConfigFile.DISCUSSION_INDEX));
		Exceptions.checkDirExists(newswireIndex, "newswire text lucene search index");
		Exceptions.checkDirExists(webIndex, "web text lucene search index");
		Exceptions.checkDirExists(discussionIndex, "discussion text lucene search index");
		Log.info(Strings.build("Lucene document index locations::\n",
			"lucene search index, newswire loc:    ", newswireIndex.getCanonicalPath(), "\n",
			"lucene search index, web loc:         ", webIndex.getCanonicalPath(), "\n",
			"lucene search index, discussion loc:  ", discussionIndex.getCanonicalPath(), "\n"));
		return new LuceneQuerierSelector(
			new Querier(FSDirectory.open(newswireIndex)),
			new Querier(FSDirectory.open(webIndex)),
			new Querier(FSDirectory.open(discussionIndex))
		);
	}

	@Override
	public Querier select(Querier.Type qt) {
		switch (qt) {
			case NEWSWIRE:
				return newswire;
			case DISCUSSION:
				return discussion;
			case WEB:
				return web;
		}
		throw new IllegalStateException(Strings.build("unrecognized querier type: ", qt));
	}

}
