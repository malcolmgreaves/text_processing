package schnell.data.index.search;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public interface QuerierSelector {


	public Querier select(Querier.Type qt);

}