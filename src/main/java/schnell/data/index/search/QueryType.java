package schnell.data.index.search;

/**
 * Indicates which type of querier to use.
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public enum QueryType {
	QNAME,
	DOC_ID,
	DOC_SENT_ID,
	FIND_DOCSENTID,
	NER,
	QNAME_AND_ANSWER;
}