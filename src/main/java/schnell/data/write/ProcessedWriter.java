package schnell.data.write;


import schnell.struct.IdedChunkedParsed;
import schnell.struct.text.TextPosNerDpOffset;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public class ProcessedWriter
    implements Writer<IdedChunkedParsed> {

  static public String tokenLineFormat_h(final String id, final TextPosNerDpOffset inputToPrint, final int ii) {
    // values are tab separated, includes all token information according to the CONnL 2005 format
    return Strings.build(
        String.valueOf(ii), "\t",                                       // sentence index of token
        inputToPrint.text(), "\t", inputToPrint
        .text(), "\t",           // the word & lemma (but we don't have lemmatized)
        inputToPrint.posTag(), "\t", inputToPrint.posTag(), "\t",       // POS tags for word (& lemmatized...)
        id, ":", inputToPrint.nerTag(), ":",                            // DOC sent ID, NER TAG, ...
        String.valueOf(inputToPrint.beg()), ":", String.valueOf(inputToPrint.end()), "\t", // OFFSET information
        String.valueOf(inputToPrint.dependencyIndex()), "\t",           // DEPENDENCY index
        inputToPrint.depParseLabel(), "\t",                             // DEPENDENCY label
        "_\t_\t" // fillers
                        );
  }

  @Override
  public void write(final BufferedWriter w, final IdedChunkedParsed input)
      throws IOException {
    Exceptions.checkNonNull(w, "writer");
    Exceptions.checkNonNull(input, "preprocessed input");

    final String id = input.docsentID();
    final List<TextPosNerDpOffset> tokens = input.chunkedAndParsed();
    for (int ii = 0; ii < tokens.size(); ii++) {
      final TextPosNerDpOffset tok = tokens.get(ii);
      // first token == # 1 (not 0)
      w.write(tokenLineFormat_h(id, tok, ii + 1));
      w.newLine();
    }
    // sentence end is marked by "\n"
    w.newLine();
  }

}