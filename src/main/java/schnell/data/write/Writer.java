package schnell.data.write;


import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Converts some object into a string. Suitable for "printing" objects to
 * a file.
 *
 * @param <X> object type
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public interface Writer<X> {

	/**
	 * Writes the input to the buffered writer. May assume that the writer writer
	 * is ready and that writer and the input are non null.
	 *
	 * @param w     non null ready writer
	 * @param input
	 * @throws IOException
	 */
	public void write(BufferedWriter w, X input) throws IOException;

}
