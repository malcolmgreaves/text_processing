package schnell.data.io;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * These functions create buffered readers and writers for text files
 * that are compressed using the GZIP format. The point is to simplify
 * writer/reader creation to one line while providing a consistent and
 * robust method for creating these writers/readers.
 * <p/>
 * <p/>
 * EXAMPLE USAGE: Reading and Writing to a GZIP file.
 * <p/>
 * BufferedWriter wtr = IO.gzipWriter("./myfile.txt.gz");
 * wtr.write("Hello World!\n");
 * wtr.close()
 * <p/>
 * BufferedReader rdr = IO.gzipReader("./myfile.txt.gz");
 * System.out.println(rdr.readLine());
 * rdr.close();
 * OUTPUT:
 * Hello World!
 *
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class IO {
	static final public String
		STDOUT_STR = "stdout",
		STDIN_STR = "stdin",
		STDERR_STR = "stderr";
	/**
	 * use in constructor for Buffered{Reader,Writer}(File,BUFSIZE in Bytes)
	 */
	static final public int
		BUFSIZE_256MB = 268435456;

	//
	// UTILITIES
	//

	// Replaces the "~" in a path with the system-dependant path to the
	// user's home directory.
	static public String replaceTilde(String location) {
		if (location.startsWith("~")) {
			return System.getProperty("user.home") + location.substring(1);
		}
		return location;
	}

	static public String readAll(String location)
		throws FileNotFoundException, IOException {
		return readAll(new File(location));
	}

	static public String readAll(File location)
		throws FileNotFoundException, IOException {
		BufferedReader br = IO.reader(location);
		StringBuilder sb = new StringBuilder();
		for (String line; (line = br.readLine()) != null; ) {
			sb.append(line).append("\n");
		}
		br.close();
		return sb.toString();
	}

	//
	// READERS
	//

	/**
	 * default buffer size is 8192 (8K)
	 *
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	static public BufferedReader reader(String path)
		throws FileNotFoundException, IOException {
		return reader(path, 8192 * 10);// 80 MB
	}

	static public BufferedReader reader(File path)
		throws FileNotFoundException, IOException {
		return reader(path.getCanonicalPath());
	}

	static public BufferedReader reader(File path, int bufSize)
		throws FileNotFoundException, IOException {
		return reader(path.getCanonicalPath(), bufSize);
	}

	/**
	 * Selects appropriate reader.
	 *
	 * @param fi
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	static public BufferedReader reader(String fi, int bufferSize)
		throws FileNotFoundException, IOException {
		Exceptions.checkNonNull(fi, "file");
		Exceptions.checkIsPositive(bufferSize, "bufferSize");

		if (fi.endsWith(".gz")) {
			return gzipReader(fi, bufferSize);

		} else if (fi.endsWith(".bz2")) {
			return bz2Reader(fi, bufferSize);
		}

		return plaintextReader(fi, bufferSize);
	}

	static public BufferedReader gzipReader(File fi, int bufferSize)
		throws IOException {
		return gzipReader(fi.getCanonicalPath(), bufferSize);
	}

	static public BufferedReader gzipReader(String fi, int bufferSize)
		throws FileNotFoundException, IOException {
		return new BufferedReader(
			new InputStreamReader(
				new GZIPInputStream(
					new FileInputStream(fi))),
			bufferSize);
	}

	static public BufferedReader bz2Reader(File fi, int bufferSize)
		throws FileNotFoundException, IOException {
		return bz2Reader(fi.getCanonicalPath(), bufferSize);
	}

	static public BufferedReader bz2Reader(String fi, int bufferSize)
		throws FileNotFoundException, IOException {
		return new BufferedReader(
			new InputStreamReader(
				new BZip2CompressorInputStream(
					new FileInputStream(fi))),
			bufferSize);
	}

	static public BufferedReader plaintextReader(File fi, int bufferSize)
		throws IOException {
		return plaintextReader(fi.getCanonicalPath(), bufferSize);
	}

	static public BufferedReader plaintextReader(String fi, int bufferSize)
		throws IOException {
		return new BufferedReader(new FileReader(fi), bufferSize);
	}

	/**
	 * @return A Buffered Reader for STDIN.
	 */
	static public BufferedReader stdin() {
		return new BufferedReader(new InputStreamReader(System.in));
	}

	/**
	 * @param s The String.
	 * @return A BufferedReader for The String.
	 */
	static public BufferedReader stringReader(String s) {
		return new BufferedReader(new StringReader(s));
	}

	//
	// WRITERS
	//

	static public BufferedWriter writer(File path)
		throws FileNotFoundException, IOException {
		return writer(path.getCanonicalPath(), 8192 * 10); //80 MB
	}

	static public BufferedWriter writer(String path)
		throws FileNotFoundException, IOException {
		return writer(path, 8192 * 10); // 80 MB
	}

	static public BufferedWriter writer(File path, int bufsize)
		throws FileNotFoundException, IOException {
		return writer(path.getCanonicalPath(), bufsize);
	}

	/**
	 * Selects correct writer.
	 *
	 * @param fi
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	static public BufferedWriter writer(String fi, int bufsize)
		throws FileNotFoundException, IOException {
		String path = fi;

		if (path.endsWith(".gz")) {
			return gzipWriter(fi, bufsize);

		} else if (path.endsWith(".bz2")) {
			return bz2Writer(fi, bufsize);
		}

		return plaintextWriter(fi, bufsize);
	}

	static public BufferedWriter gzipWriter(String fi, int bufsize)
		throws FileNotFoundException, IOException {
		return new BufferedWriter(
			new OutputStreamWriter(
				new GZIPOutputStream(
					new FileOutputStream(fi))),
			bufsize);
	}

	static public BufferedWriter bz2Writer(String fi, int bufsize)
		throws FileNotFoundException, IOException {
		return new BufferedWriter(
			new OutputStreamWriter(
				new BZip2CompressorOutputStream(
					new FileOutputStream(fi))),
			bufsize);
	}

	static public BufferedWriter plaintextWriter(String fi, int bufsize)
		throws FileNotFoundException, IOException {
		return new BufferedWriter(new FileWriter(fi), bufsize);
	}

	// standard error and output streams
	static public BufferedWriter stdout() {
		return new BufferedWriter(new OutputStreamWriter(System.out));
	}

	static public BufferedWriter stderr() {
		return new BufferedWriter(new OutputStreamWriter(System.err));
	}

	// for path joinging
	static public String pathJoin(String... pathParts) {
		Exceptions.checkNonNull(pathParts, "path parts to join");
		StringBuilder sb = new StringBuilder();

		for (String part : pathParts) {
			if (!part.endsWith(File.separator)) {
				part = part + File.separator;
			}
			sb.append(part);
		}
		Strings.rmLastSubstr(sb, File.separator);

		return sb.toString();
	}

	// for path joinging
	static public File pathJoin(File base, String... pathParts)
		throws IOException {
		Exceptions.checkDirExists(base, "base directory");
		String path = base.getCanonicalPath();
		path = path.endsWith(File.separator) ? path + File.separator : path;
		return new File(path + pathJoin(pathParts));
	}

}