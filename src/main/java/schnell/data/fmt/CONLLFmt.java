package schnell.data.fmt;

import java.util.List;

import schnell.struct.StringArrayFormatter;
import schnell.struct.text.TextPosNer;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * 
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 *
 */
public class CONLLFmt
implements StringArrayFormatter<List<TextPosNer>> {

	@Override
	public String[] format(List<TextPosNer> tagged){
		Exceptions.checkNonNull(tagged, "POS and NER tagged words");
		int size = tagged.size();
		String[] formatted = new String[size];

		// format the list of tagged words in CONLL format
		TextPosNer t;
		for(int ii = 0; ii < size; ii++){
			t = tagged.get(ii);

			formatted[ii] = String.format("%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s", 
					ii+1,                      // the token's index
					t.text(),                  // the string
					t.text().toLowerCase(),    // the stemmed string.....not here though...
					t.posTag(),                // the POS
					t.posTag(),                // the stemmed POS........not here though...
					// the Document ID, along with offset information & NER tag
					Strings.build("x:y:",t.nerTag(),":-1:-1"),
					"_",                       // index of token that this one depends on (not known at this time)
					"_",                       // dependency label (not known at this time)
					"_",                       // unknown -- I have no idea what this is for
					Strings.NEWLINE);          // always need newline character
		}

		return formatted;
	}

}