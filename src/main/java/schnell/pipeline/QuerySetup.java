package schnell.pipeline;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import schnell.data.index.search.QueryType;
import schnell.run.ReadConfigFile;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public class QuerySetup {

	final public QueryType qt;
	final public int maxDocs, maxTokSafe;

	public QuerySetup(QueryType qt, int maxDocs, int maxTokSafe) {
		Exceptions.checkNonNull(qt, "query type");
		Exceptions.checkIsPositive(maxDocs, "maximum documents to return per query");
		Exceptions.checkIsPositive(maxTokSafe, "maximum length of each returned document");
		this.qt = qt;
		this.maxDocs = maxDocs;
		this.maxTokSafe = maxTokSafe;
	}

	static public QuerySetup make(Configuration conf, Logger log) {
		QueryType qt = conf.getEnum(ReadConfigFile.QUERY_TYPE, QueryType.QNAME);
		int maxDocs = conf.getInt(ReadConfigFile.MAX_DOCS, 250);
		if (maxDocs < 1) {
			maxDocs = 3000;
		}
		int maxTokSafe = conf.getInt(ReadConfigFile.MAX_TOK_SAFE, 300);
		if (maxTokSafe < 1) {
			maxTokSafe = 300;
		}
		log.info(Strings.build("Query configuration:\n",
			"Query Type:                           ", qt, "\n",
			"Max # documents to return:            ", maxDocs, "\n",
			"Maximum length of returned doc:       ", maxTokSafe, "\n"
		));

		return new QuerySetup(qt, maxDocs, maxTokSafe);
	}

}