package schnell.pipeline.test;

import com.beust.jcommander.internal.Lists;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import schnell.data.index.search.KBPQuery;
import schnell.data.index.search.Querier;
import schnell.data.index.search.QuerierSelector;
import schnell.pipeline.QuerySetup;
import schnell.process.ConverterExcept;
import schnell.struct.doc.DocLuceneScore;
import schnell.struct.doc.ScoredDocument;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public class KBPQueriesToDocuments
    implements ConverterExcept<KBPQuery, List<ScoredDocument>, Exception> {

  protected Logger Log;
  protected QuerierSelector qs;
  protected QuerySetup querySetup;


  public KBPQueriesToDocuments(Logger log, QuerierSelector qs, QuerySetup qsetup) {
    Exceptions.checkNonNull(log, "log");
    this.Log = log;
    setQuerierSelector(qs);
    setQuerySetup(qsetup);
  }

  public void setQuerySetup(QuerySetup q) {
    Exceptions.checkNonNull(q, "query setup");
    this.querySetup = q;
  }

  public QuerierSelector querierSelector() {
    return qs;
  }

  public void setQuerierSelector(QuerierSelector qs) {
    Exceptions.checkNonNull(qs, "querier selector");
    this.qs = qs;
  }

  @Override
  public List<ScoredDocument> convert(KBPQuery kbpQuery)
      throws Exception {

    // get the appropriate querrrier -- usually a specific lucene search index
    Querier querier = qs.select(kbpQuery.qtype);

    // This is larger than max docs because we will remove dupliconvertcates
    // the *4 is a heuristic
    final int nDocsToSearchFor = querySetup.maxDocs;
    // search for the term (query text or doc ID) in the index,
    // store result in hits
    DocLuceneScore[] hits;
    switch (querySetup.qt) {
      case QNAME:
        if (Log.isDebugEnabled()) {
          Log.debug("searching for text: " + kbpQuery.queryText);
        }
        hits = querier.queryName(Strings.build("\"", kbpQuery.queryText, "\""), nDocsToSearchFor);
        DocLuceneScore[] exactHits = hits,
            allHits = querier.queryName(Strings.build(kbpQuery.queryText), nDocsToSearchFor);
        hits = new DocLuceneScore[exactHits.length + allHits.length];
        System.arraycopy(exactHits, 0, hits, 0, exactHits.length);
        System.arraycopy(allHits, 0, hits, exactHits.length, allHits.length);
        Log.info(Strings.build("got ", exactHits.length, " results searching on exact query text; got ",
                               hits.length, " on all words in text"));

        break;
      case DOC_ID:
        if (Log.isDebugEnabled()) {
          Log.debug("searching for doc ID: " + kbpQuery.docID);
        }
        hits = querier.queryDocID(kbpQuery.docID, nDocsToSearchFor);
        break;
      default:
        hits = searchDefaultCase(kbpQuery);
        if (hits == null) {
          throw new IllegalStateException("Invalid querrier type for this program: " +
                                          querySetup.qt);
        }
        break;
    }

    // condense hits that are the same until we have at most savedHits
    // hits (search results)
    int hitsProcessed = 0;
    Set<String> savedHits = Sets.newHashSet();

    List<ScoredDocument> savedHitsList = Lists.newArrayList(querySetup.maxDocs);

    Pattern[] filters = new Pattern[] {
        Pattern.compile("<(.*)>"),
        Pattern.compile("&lt;"),
        Pattern.compile("<"),
        Pattern.compile("http://(.*)[ ]")
    };
    String hitText;

    for (int hi = 0; hi < hits.length && savedHits.size() < querySetup.maxDocs; hi++) {

      hitText = hits[hi].text();
      for (Pattern filter : filters) {
        hitText = filter.matcher(hitText).replaceAll("");
      }

      // accept the document iff the document is more than just the query
      // text and if the document is not too long (which is an indicator that
      // it is a sentence segmentation error)
      if (hitText.length() > kbpQuery.queryName.length()
          && hitText.length() <= querySetup.maxTokSafe
          && !savedHits.contains(hitText)) {
        hitsProcessed++;
        savedHits.add(hitText);
        savedHitsList.add(new ScoredDocument(
            hits[hi].docsentID(),
            hits[hi].score(),
            hitText
        ));
      }
    }

    // do some text processing (POS & NE tagging, dependency parsing, etc.)
    // on the hits we got back
    Log.info(Strings.build(kbpQuery.queryName, " returning ", savedHitsList.size(), " documents (ignoring ",
                           (hitsProcessed - savedHitsList.size()), " duplicates)"));

    return savedHitsList;
  }

  protected DocLuceneScore[] searchDefaultCase(KBPQuery kbpQuery)
      throws InterruptedException, ExecutionException, ParseException, IOException {
    return null;
  }

}