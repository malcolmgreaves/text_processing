package schnell.pipeline.test;/**
 *
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public interface Interface {


	static public interface QueryIDer {
		public String queryID();
	}


}
