package schnell.pipeline.test;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import schnell.data.index.search.KBPQuery;
import schnell.data.index.search.Querier;
import schnell.data.index.search.QuerierSelector;
import schnell.pipeline.QuerySetup;
import schnell.struct.doc.DocLuceneScore;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public class KBPQueriesToDocuments_WithAns
	extends KBPQueriesToDocuments {

	protected Map<String, List<String>> id2answers;

	public KBPQueriesToDocuments_WithAns(Logger log, QuerierSelector qs, QuerySetup qsetup,
	                                     Map<String, List<String>> id2answers) {
		super(log, qs, qsetup);
		setId2answers(id2answers);
	}

	public void setId2answers(Map<String, List<String>> id2answers) {
		Exceptions.checkNonNull(id2answers, "id -> search terms (w/ answers)");
		this.id2answers = id2answers;
	}

	@Override
	protected DocLuceneScore[] searchDefaultCase(KBPQuery kbpQuery)
		throws InterruptedException, ExecutionException, ParseException, IOException {
		switch (querySetup.qt) {
			case QNAME_AND_ANSWER:
				if (Log.isDebugEnabled()) {
					Log.debug("searching for query & answer: " + kbpQuery.queryName);
				}
				final Querier querier = qs.select(kbpQuery.qtype);
				final String queryTextS = Strings.build("\"", kbpQuery.queryText, "\"");
				final int
					nDocsToSearchFor = querySetup.maxDocs * 4,
					scaled_nDocsToSearchFor = Math.max(nDocsToSearchFor / id2answers.get(kbpQuery.queryName).size(), 5);
//					scaled_nDocsToSearchFor = nDocsToSearchFor / id2answers.get(kbpQuery.queryName).size();
				final List<DocLuceneScore> hitsCollect = Lists.newArrayListWithCapacity(nDocsToSearchFor);

				DocLuceneScore[] hits;
				for (String answer : id2answers.get(kbpQuery.queryName)) {
					hits = querier.queryNameAndAnswer(queryTextS, Strings.build("\"", answer, "\""), scaled_nDocsToSearchFor);
//					hits = querier.queryNameAndAnswer(Strings.build("\"", kbpQuery.queryText, "\""),
//						Strings.build("\"", answer, "\""), scaled_nDocsToSearchFor);
					hitsCollect.addAll(Arrays.asList(hits));

					if (Log.isDebugEnabled() || true) {
						Log.info(Strings.build("received ", hits.length, " hits using answer \"",
							answer, "\" for query \"", kbpQuery.queryText, "\""));
					}
				}

				Collections.sort(hitsCollect, new Comparator<DocLuceneScore>() {
					@Override
					public int compare(final DocLuceneScore o1, final DocLuceneScore o2) {
						double diff = o1.score() - o2.score();
						if (diff < 0.0) {
							return -1;
						} else if (diff > 0.0) {
							return 1;
						}
						return 0;
					}
				});
				hits = new DocLuceneScore[hitsCollect.size()];
				return hitsCollect.toArray(hits);
		}
		return null;
	}

}