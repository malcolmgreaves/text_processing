package schnell.pipeline.test;

import com.beust.jcommander.internal.Lists;
import org.apache.log4j.Logger;
import schnell.concurrent.PutChanTerminableThread;
import schnell.concurrent.WritableWork;
import schnell.concurrent.WriterThread;
import schnell.data.write.Writer;
import schnell.struct.doc.ScoredDocument;
import schnell.struct.ds.PutChannel;
import schnell.util.Strings;
import schnell.util.Sys;
import schnell.util.except.Exceptions;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;


/**
 * @author Malcolm Greaves (greaves.malcolm@gmail.com)
 */
public abstract class ConcurrentMakeSlotfillInstances<T extends Interface.QueryIDer> {
	static protected Logger Log = Logger.getLogger(ConcurrentMakeSlotfillInstances.class);
	protected int nThreads;

	public ConcurrentMakeSlotfillInstances(int nThreads) {
		setNThreads(nThreads);
	}

	public void setNThreads(int nThreads) {
		this.nThreads = nThreads < 1 ? 1 : nThreads;
	}

	public int nThreads() {
		return nThreads;
	}

	public abstract Iterator<DocumentsQuery> getDocumentsByQuery();

	protected abstract PutChanTerminableThread<DocumentsQuery, T> threadMaker(PutChannel<T> outputChannel);

	public void run() {

		// controls writing output results from work
		final WriterThread<T> writerThread = writerThread();
		final PutChannel<T> outputChannel = new PutChannel<T>() {
			@SuppressWarnings("unchecked")
			@Override
			public void put(final T input) {
				try {
					writerThread.submit(new WritableWork<T>(
						true,
						outputBufferedWriter(input.queryID()),
						Lists.newArrayList(input)
					));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};
		writerThread.start();

		// initialze & start workers
		final List<PutChanTerminableThread<DocumentsQuery, T>> workers = Lists.newArrayList(nThreads);
		for (int wi = 0; wi < nThreads; wi++) {
			PutChanTerminableThread<DocumentsQuery, T> worker = threadMaker(outputChannel);
			workers.add(worker);
			worker.start();
		}

		System.gc();
		Log.info(Strings.build("intialized and started all ", workers.size(),
			" workers + 1 writer thread + 1 reader thread\n", Sys.memoryUsedStr()));

		// for each "logical" query, get all documents associated with it
		final Iterator<DocumentsQuery> pqs = getDocumentsByQuery();
		// process these documents in parallel
		int wi = 0;
		DocumentsQuery docsQuery;
		while (pqs.hasNext()) {
			docsQuery = pqs.next();
			try {
				// asynchronously perform the action on the query's documents
				workers.get(wi++ % nThreads).submit(docsQuery);

			} catch (InterruptedException e) {
				Log.error(Strings.build("error submitting document for work: ",
					docsQuery), e);
			}
		}

		// shutdown workers
		for (PutChanTerminableThread<DocumentsQuery, T> worker : workers) {
			try {
				worker.terminate();
				Log.info("worker safely completed");
			} catch (InterruptedException e) {
				Log.error(Strings.build("failed to terminate worker: ", worker, e));
			}
		}

		// shutdown writer
		try {
			writerThread.terminate();
			Log.info("writer thread safely completed");
		} catch (InterruptedException e) {
			Log.error("failed to terminate writer thread", e);
		}
	}

	/**
	 * Will be called asynchronoyusly.
	 */
	public abstract BufferedWriter outputBufferedWriter(String name)
		throws IOException;

	/**
	 * Will be called once per invocation of writerThread()
	 */
	public abstract Writer<T> outputWriter();

	/**
	 * Will be called once per invocation of run().
	 */
	public WriterThread<T> writerThread() {
		return new WriterThread<T>(nThreads, outputWriter());
	}

	static public class DocumentsQuery {
		final public String name;
		final public List<ScoredDocument> documents;

		public DocumentsQuery(String name, List<ScoredDocument> documents) {
			Exceptions.checkNonNullNonEmpty(name, "name");
			this.name = name;
			Exceptions.checkNonNull(documents, "documents");
			this.documents = documents;
		}

		@Override
		public String toString() {
			return Strings.build(name, "\n", Strings.join("\n\t", documents));
		}
	}

}