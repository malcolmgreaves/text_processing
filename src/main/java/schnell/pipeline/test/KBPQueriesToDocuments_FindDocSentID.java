package schnell.pipeline.test;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import schnell.data.index.search.KBPQuery;
import schnell.data.index.search.Querier;
import schnell.data.index.search.QuerierSelector;
import schnell.pipeline.QuerySetup;
import schnell.struct.doc.DocLuceneScore;
import schnell.util.Strings;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * Author: Malcolm W. Greaves (greaves.malcolm@gmail.com)
 */
public class KBPQueriesToDocuments_FindDocSentID
    extends KBPQueriesToDocuments {

  public KBPQueriesToDocuments_FindDocSentID(Logger log, QuerierSelector qs, QuerySetup qsetup) {
    super(log, qs, qsetup);
  }

  protected DocLuceneScore[] searchDefaultCase(KBPQuery kbpQuery)
      throws InterruptedException, ExecutionException, ParseException, IOException {
    switch (querySetup.qt) {
      case FIND_DOCSENTID:
        final Querier querier = qs.select(kbpQuery.qtype);
        DocLuceneScore[] hits = querier.queryName(Strings.build("\"", kbpQuery.queryText, "\""),
                                                  querySetup.maxDocs * 4);
        Set<String> docIDs = Sets.newHashSetWithExpectedSize(hits.length);
        for (DocLuceneScore hit : hits) {
          docIDs.add(hit.docID());
        }

        List<DocLuceneScore> allhits = Lists.newArrayListWithExpectedSize(hits.length);
        for (String docID : docIDs) {
          if (Log.isDebugEnabled()) {
            Log.debug("getting results for doc: " + docID);
          }
          Collections.addAll(allhits, querier.queryDocID(docID, querySetup.maxDocs));
        }


        hits = new DocLuceneScore[allhits.size()];
        return allhits.toArray(hits);

      default:
        return super.searchDefaultCase(kbpQuery);
    }
  }

}