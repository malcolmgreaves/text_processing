package schnell.pipeline.test;

import schnell.struct.IdedChunkedParsed;
import schnell.util.Strings;
import schnell.util.except.Exceptions;

import java.util.List;

/**
 * @author Malcolm W. Greaves (greaes.malcolm@gmail.com)
 */
public class ProcessedQuery
	implements Interface.QueryIDer {
	final public String queryID;
	final public List<IdedChunkedParsed> processedSentences;

	public ProcessedQuery(String queryID, List<IdedChunkedParsed> processedSentences) {
		Exceptions.checkNonNullNonEmpty(queryID, "query ID");
		this.queryID = queryID;
		Exceptions.checkNonNull(processedSentences, "processed sentences");
		this.processedSentences = processedSentences;
	}

	@Override
	public String queryID() {
		return queryID;
	}

	@Override
	public String toString() {
		return Strings.build(queryID, "\n", Strings.join("\n\t", processedSentences));
	}

}